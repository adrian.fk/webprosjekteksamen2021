# Getting started

## Required Prerequisites
- (Pull git repository)
- Install NPM


## 1. Install dependencies
After considering the required prerequisites, open a terminal instance and cd into the cloned
directory. Afterwards run the following npm command, to install all the external dependencies utilized
in this project.

### `npm install`

Installs the listed dependencies specified in package.json

## 2. Start

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

