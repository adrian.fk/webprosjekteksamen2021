import React, {Component} from 'react';
import MenuController from "../../../../infrastructure/controllers/MenuController";
import Strings from "../../../Strings/Strings";

class EditMenuView extends Component {
    constructor(props = {foodId: ""}, context) {
        super(props, context);

        this.id = "EditMenuView";
        this.className = "editMenu";

        if (props.foodId === "") {
            console.error(this.foodId + " attempted to be instantiated without a propprietary ID")
        }

        this.state = {
            foodId: props.foodId,
            name: "",
            price: "",
            dishDescription: "",
            imgUrl: ""
        }
    }

    componentDidMount() {
        MenuController().initEditMenu(this.state.foodId, this)
    }

    updateViewState(foodId, dishName, price, dishDescription, imgUrl) {
        this.setState({
            foodId,
            name: dishName,
            price,
            dishDescription,
            imgUrl
        })
    }

    onExitEditMenuView() {
        MenuController().onExitEditMenuView();
    }

    onInputImgUrlChanged(e) {
        this.setState({
            imgUrl: e.target.value
        });
    }

    onInputNameChanged(e) {
        this.setState({
            name: e.target.value
        });
    }

    onInputPriceChanged(e) {
        this.setState({
            price: e.target.value
        });
    }

    onInputDishDescriptionChanged(e) {
        this.setState({
            dishDescription: e.target.value
        });
    }

    updateBtnClicked (e) {
        e.preventDefault();
        MenuController().onUpdateMenuEntry(
            this.state.foodId,
            this.state.name,
            this.state.price,
            this.state.dishDescription,
            this.state.imgUrl
        )
    }

    render() {
        return (
            <div id={this.id} className={this.className + "--wrapper"} >
                <div
                    id={this.id}
                    className={this.className + "__background"}
                    onClick={() => this.onExitEditMenuView()}
                >

                    <form className={this.className}
                          onSubmit={(e) => this.updateBtnClicked(e)}
                    >
                        <div className="card" onClick={e => e.stopPropagation()}>
                            <div className="card-image">
                                <figure className="image is-4by3 is-rounded">
                                    <img src={this.state.imgUrl} alt="image" />
                                </figure>
                            </div>
                            <div className="card-content">
                                <div className="media">
                                    <div className="media-content">
                                        <label>{Strings.MENU_EDIT_IMG_URL()}</label>
                                        <input
                                            className="input is-4 is-rounded"
                                            placeholder={Strings.MENU_EDIT_IMG_URL()}
                                            value={this.state.imgUrl}
                                            onChange={(e) => this.onInputImgUrlChanged(e)}
                                        />
                                        <label>{Strings.MENU_EDIT_NAME()}</label>
                                        <input
                                            className="input is-4 is-rounded"
                                            placeholder={Strings.MENU_EDIT_NAME()}
                                            value={this.state.name}
                                            onChange={e => this.onInputNameChanged(e)}
                                        />
                                    </div>
                                </div>

                                <div className="content">
                                    <label>{Strings.MENU_EDIT_PRICE()}</label>
                                    <input
                                        className={"input is-rounded"}
                                        placeholder={Strings.MENU_EDIT_PRICE()}
                                        value={this.state.price}
                                        onChange={e => this.onInputPriceChanged(e)}
                                    />
                                    <label>{Strings.MENU_EDIT_DESCRIPTION()}</label>
                                    <input
                                        className={"input is-rounded"}
                                        placeholder={Strings.MENU_EDIT_DESCRIPTION()}
                                        value={this.state.dishDescription}
                                        onChange={e => this.onInputDishDescriptionChanged(e)}
                                    />

                                    <button
                                        className={"button is-medium is-fullwidth is-black is-rounded"}
                                        type={"submit"}
                                    >
                                        {Strings.MENU_EDIT_BTN_SUBMIT()}
                                    </button>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default EditMenuView;