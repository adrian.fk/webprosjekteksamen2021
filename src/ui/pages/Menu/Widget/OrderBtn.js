import React, { Component } from 'react'
import MenuController from '../../../../infrastructure/controllers/MenuController';
import Strings from '../../../Strings/Strings';

export default class OrderBtn extends Component {

    constructor(props, context) {
        super(props, context)
        this.state = {
            restaurantId: props.restaurantId,
            foodId: props.foodId,
            dishName: props.dishName,
            price: props.price, 
            dishDescription: props.dishDescription, 
            imgUrl: props.imgUrl
        }
    }

    _handleShoppingCartBtn(e) {
        e.stopPropagation();
        MenuController().onAddShoppingCart(this.state.foodId, this.state.dishName, this.state.price, this.state.dishDescription, this.state.imgUrl, this.state.restaurantId)
    }



    render() {
        return (
            <div>
                <button 
                    className={"button is-fullwidth is-black is-rounded"}
                    onClick={e => this._handleShoppingCartBtn(e)}
                >
                    {Strings.getMenuCardAddToShoppingcart()}
                </button>
            </div>
        )
    }
}
