import React, { Component } from 'react'
import MenuController from '../../../../infrastructure/controllers/MenuController';
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import Strings from '../../../Strings/Strings';

export default class DropDownMenu extends Component {

    static activeDropDownStateTrigger = false;




    constructor(props){
        super(props)
        this.className = "dropDownMenu"
        this.state = {
            dropDownExpanded: false,
            restaurants: [],
            activeRestaurant: Strings.getMenuRestaurantSelectorTitle(),
            restaurantId: props.restaurantId
        }
    }

    componentDidMount() {
        MenuController().initRestaurantsDropDown(this)
        const dropDown = document.querySelector("." + this.className);
        dropDown.style.opacity = 0;
        window.addEventListener(
            "scroll",
            () => {
                const pageOffset = window.pageYOffset;
                if(pageOffset >= 0) {
                    dropDown.style.opacity = (pageOffset)/100;
                }
            }
        )
    }

    componentDidUpdate() {
        if(!DropDownMenu.activeDropDownStateTrigger && this.state.restaurantId === "") {
            console.log(this.state.restaurantId);
            DropDownMenu.activeDropDownStateTrigger = true;
            this.setState({activeRestaurant: this.state.restaurantId !== "" ? this.state.activeRestaurant : Strings.getMenuRestaurantSelectorTitle()})
        }
        else{
            DropDownMenu.activeDropDownStateTrigger = false;
        }
    }

    clearViewState() {
        this.setState( {
            restaurants: [],
        });
    }

    displayRestaurantsDropdown(restaurants, currentlySelectedRestaurant){
        console.log(currentlySelectedRestaurant);
        this.clearViewState();
        this.setState( {
            restaurantId: currentlySelectedRestaurant?.restaurantId || "",
            activeRestaurant: currentlySelectedRestaurant?.name || this.state.activeRestaurant,
            restaurants
        });
    }

    onDropDownClicked(){
        if(this.state.dropDownExpanded) {
            document.querySelector(".dropdown")?.classList.remove("is-active")
        }
        else {
            document.querySelector(".dropdown")?.classList.add("is-active")
        }
        this.setState({dropDownExpanded: !this.state.dropDownExpanded})
    }

    onDropDownEntryClicked(restaurant, e) {
        e.preventDefault();
        document.getElementById(this.state.restaurantId)?.classList.remove("is-active")
        this.setState({activeRestaurant: restaurant.name, restaurantId: restaurant?.restaurantId || ""})
        this.onDropDownClicked();
        document.getElementById(restaurant.restaurantId)?.classList.add("is-active")
        MenuController().setRestaurantId(restaurant.restaurantId);
    }

    render() {
        return (
            <div className={this.className + "__wrapper"}>
                <div className={this.className + " dropdown"} onClick={() => this.onDropDownClicked()}>
                    <div class="dropdown-trigger">
                        <button class="button is-rounded" aria-haspopup="true" aria-controls="dropdown-menu">
                        <span>{this.state.activeRestaurant}</span>
                        <span class="icon is-small">
                            <ArrowDropDownIcon aria-hidden={"true"} />
                        </span>
                        </button>
                    </div>
                    <div class="dropdown-menu" id="dropdown-menu" role="menu">
                        <div class="dropdown-content">
                            {this.state.restaurants.map(
                                restaurant => 
                                <a id={restaurant.restaurantId} href="#" class="dropdown-item" onClick={(e) => this.onDropDownEntryClicked(restaurant, e)}>
                                    {restaurant.name}
                                </a>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}
