import React, {Component} from 'react';
import MenuController from "../../../../infrastructure/controllers/MenuController";
import Strings from '../../../Strings/Strings';
import ConfirmBtns from './ConfirmBtns';
import OrderBtn from './OrderBtn';

class MenuView extends Component {

    static BTN_STATE_ADD_SHOPPINGCART = Object.freeze(true);
    static BTN_STATE_REMOVE_SHOPPINGCART = Object.freeze(false);




    constructor(props, context) {
        super(props, context);

        this.className = "menuView"

        this.state = {
            restaurantId: props.restaurantId,
            foodId: props.foodId,
            dishName: props.dishName,
            price: props.price,
            dishDescription: props.dishDescription,
            imgUrl: props.imgUrl,
            existsInCart: props.existsInCart,

            shoppingCartBtnState: MenuView.BTN_STATE_ADD_SHOPPINGCART,
            shoppingCartBtnTxt: MenuView.BTN_STATE_ADD_TXT
        }
    }

    _handleShoppingCartBtn(e) {
        e.stopPropagation()
        if(this.state.shoppingCartBtnState){
            MenuController().onAddShoppingCart();
            this.setState({
                shoppingCartBtnState: MenuView.BTN_STATE_REMOVE_SHOPPINGCART,
                shoppingCartBtnTxt: MenuView.BTN_STATE_REMOVE_TXT
            })
        }
        else{
            MenuController().onRemoveShoppingCart(this.state.foodId);
            this.setState({
                shoppingCartBtnState: MenuView.BTN_STATE_ADD_SHOPPINGCART,
                shoppingCartBtnTxt: MenuView.BTN_STATE_ADD_TXT
            })
        }
    }

    render() {
        return (
            <div
                className={this.className + " column is-3"}
                onClick={(e) => MenuController().onMenuClicked(this.state.foodId)}
            >
                <div className="card">
                    <div className="card-image">
                        <figure className="image is-4by3">
                            <img src={this.state.imgUrl} alt="image" />
                        </figure>
                    </div>
                    <div className="card-content">
                        <div className="media">
                            <div className="media-content">
                                <p className="title is-4">{this.state.dishName}</p>
                                <label>{Strings.getMenuCardPrice()}</label>
                                <p className="title is-6">{this.state.price} Kr.</p>
                                <label>{Strings.getMenuCardDescription()}</label>
                                <p className="title is-6">{this.state.dishDescription}</p>
                                {this.state.existsInCart ? 
                                    < ConfirmBtns foodId={this.state.foodId} /> 
                                    : 
                                    <OrderBtn 
                                        foodId={this.state.foodId} 
                                        dishName={this.state.dishName} 
                                        price={this.state.price} 
                                        dishDescription={this.state.dishDescription} 
                                        imgUrl={this.state.imgUrl} 
                                        restaurantId={this.state.restaurantId}
                                    />
                                }
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default MenuView;