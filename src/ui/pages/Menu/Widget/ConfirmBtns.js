import React, { Component } from 'react'
import { Redirect } from 'react-router'
import MenuController from '../../../../infrastructure/controllers/MenuController'
import Strings from '../../../Strings/Strings';

export default class ConfirmBtns extends Component {

    constructor(props) {
        super(props);
        this.className = "confirmBtns"
        this.state = {
            navToCheckout: false
        }
    }

    onRemoveBtnClicked() {
        MenuController().onRemoveShoppingCart(this.props.foodId)
    }

    onCheckoutbtnClicked() {
        this.setState({navToCheckout: true})
    }

    render() {
        if(this.state.navToCheckout)
            return (<Redirect push to={"/checkout"}/>)
        return (
            <div className={this.className}>
                <div className={this.className + "__btn"}>
                    <button className={"button is-fullwidth is-light is-rounded"} onClick={() => this.onRemoveBtnClicked()}>{Strings.getMenuCardRemoveFromShoppingcart()}</button>
                </div>
                <div className={this.className + "__btn"}>
                    <button className={this.className + "__btn button is-fullwidth is-black is-rounded"}  onClick={() => this.onCheckoutbtnClicked()}>{Strings.getMenuCardGoToShoppingcart()}</button>
                </div>
            </div>
        )
    }
}
