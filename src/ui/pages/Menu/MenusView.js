import React from 'react';
import Page from "../../component/Page";
import HeaderView from "../../component/Header/HeaderView";
import HeroSection from "../../component/HeroSection/HeroSection";
import MenuView from "./Widget/MenuView";
import MenuController from "../../../infrastructure/controllers/MenuController";
import EditMenuView from "./Widget/EditMenuView";
import RestaurantMenuIcon from '@material-ui/icons/RestaurantMenu';
import Strings from '../../Strings/Strings';
import Link from 'react-router-dom/Link';
import DropDownMenu from './Widget/DropDownMenu';


import '../../style/Menu.css'

class MenusView extends Page {

    constructor(props, context) {
        super(props, context);

        this.id = "Menu"
        this.className = "menu"

        this.state = {
            restaurantId: "",
            menu: [],
            displayEditMenuView: false,
            displayEditMenuViewId: ""
        }
    }

    componentDidMount() {
        MenuController().init(this);
    }
    
    componentDidUpdate() {
        HeroSection.setTitle(Strings.getMenuHeroTitle());
        HeroSection.clearBtns();
        HeroSection.addButton({
           id: "1",
           text: Strings.getMenuHeroOverview(),
           linkTo: "#",
           onClickHandler: (e) => {
               this.scrollToMenuOverview();
           }
       });
       HeroSection.addButton({
           id: "2",
           text: Strings.getMenuSeShoppingcart(),
           linkTo: "/checkout"
       });
       HeroSection.addButton({
           id: "3",
           text: Strings.getMenuHeroAddDish(),
           linkTo: "#",
           onClickHandler: (e) => {
               this.addBtnClicked();
           }
       })
    }

    scrollToMenuOverview() {
        document.getElementById(this.id)?.scrollIntoView({
            behaviour: "smooth",
        });
    }

    clearMenu(){
        this.setState({
            displayEditMenuView: false,
            menu: []
        })
    }

    displayMenu(menu = [{foodId: "", dishName: "", price: "", dishDescription: "", imgUrl: "", existsInCart: false}], curCartRestaurantId) {
        this.clearMenu();
        console.log(menu.curCartRestaurantId)
        this.setState({menu, restaurantId: curCartRestaurantId});
    }

    displayEditMenuView(id) {
        this.setState({
            displayEditMenuViewId: id,
        })
        this.setState({
            displayEditMenuView: true
        })
    }

    hideEditMenuView() {
        this.setState({
            displayEditMenuView: false,
            displayEditMenuViewId: ""
        })
    }

    addBtnClicked() {
        MenuController().onAddMenuEntry()
    }

    rerender() {
        MenuController().fetchMenu();
    }

    updateRestaurantId(restaurantId) {
        this.setState({restaurantId})
    }

    render() {
        return (
            <section className={this.className}>
                <HeaderView activePageName={HeaderView.ACTIVE_PAGE_MENU} />
                <HeroSection
                    title={"Vår meny"}
                    imgUrl={"https://images.unsplash.com/photo-1600346019001-8d56d1b51d59?ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=1650&q=80"}
                />
                <section id={this.id} className={this.className + "__container"}>
                    <div className={this.className + "__title-container"}>
                        <div className={this.className + "__title--wrapper"}>
                            <ul>
                                <li>
                                    <Link className={this.className + "__icon--wrapper"} to="#" onClick={() => this.scrollToMenuOverview()}>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <span></span>
                                    <RestaurantMenuIcon className={this.className + "__menuIcon"} />
                                    <h2 className={this.className + "__titleTxt"}>{Strings.getMenuOverviewTitle()}</h2>
                                    </Link>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div className={this.className + "__restaurantDropDown--wrapper"}>
                        <DropDownMenu restaurantId={this.state.restaurantId}/>
                    </div>
                    <section className={this.className + " columns is-multiline has-12"}>
                        {this.state.restaurantId !== "" ? this.state.menu.map(
                            (menu) => {
                                return <MenuView
                                    restaurantId={this.state.restaurantId}
                                    foodId={menu.foodId}
                                    dishName={menu.dishName}
                                    imgUrl={menu.imgUrl}
                                    price={menu.price}
                                    dishDescription={menu.dishDescription}
                                    existsInCart={menu.existsInCart}
                                />
                            }
                        ) : ""}
                    </section>
                </section>
                { this.state.displayEditMenuView ? <EditMenuView foodId={this.state.displayEditMenuViewId} /> : ""}
            </section>
        );
    }
}

export default MenusView;