import React, {Component} from 'react';

import Logo from "../../../../assets/images/logo-fixed.svg";
import {Link} from "react-router-dom";

class HomeNavBar extends Component {
    render(){
        return (
            <div className={"home_headerNavigation"}>
                <div className={"home_headerLogo"}>
                    <img
                        src={Logo}
                        alt={"Gyldne Pizza logo"}
                    />
                    <h3>Gyldne Pizza</h3>
                </div>

                <div className={"home_headerNavigationElements"}>
                    <nav className={"home_headerNavigationItems"}>
                        <nav className="homeNavigation_menuItem homeNavigation_home">
                            <Link to={"/home"}>HOME</Link>
                        </nav>
                        <nav className="homeNavigation_menuItem homeNavigation_restaurants">
                            <Link to={"/restaurants"}>RESTAURANTS</Link>
                        </nav>
                        <nav className="homeNavigation_menuItem homeNavigation_employees">
                            <Link to={"/employees"}>EMPLOYEES</Link>
                        </nav>
                        <nav className="homeNavigation_menuItem homeNavigation_menu">
                            <Link to={"/menu"}>MENU</Link>
                        </nav>
                        <nav className="homeNavigation_menuItem homeNavigation_checkout">
                            <Link to={"/checkout"}>CHECKOUT</Link>
                        </nav>
                        <nav className="homeNavigation_menuItem homeNavigation_statistics">
                            <Link to={"/statistics"}>STATISTICS</Link>
                        </nav>
                    </nav>
                </div>
            </div>

        );
    }
}

export default HomeNavBar;