import React, {Component} from 'react';
import {Link} from "react-router-dom";

class adminBtn extends Component {
    render(){
        return(
            <div className={"home_btn"}>
                <Link className={"home_adminBtn"} to={"/statistics"}>
                    <nav>
                        <h5>ADMIN</h5>
                    </nav>
                </Link>
            </div>
        );
    }
}

export default adminBtn;