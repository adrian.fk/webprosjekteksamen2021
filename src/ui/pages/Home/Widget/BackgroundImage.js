import React, {Component} from 'react';
import backroundImg from "../../../../assets/images/triangle.svg"

export default
class BackgroundImg extends Component {
    render(){
        return(
            <div className={"backgroundImage--wrapper"}>
                <div className={"backgroundImage"}>
                    <img
                        src={backroundImg}
                        alt={"triangle background"}
                    />
                </div>
            </div>
        );
    }
}