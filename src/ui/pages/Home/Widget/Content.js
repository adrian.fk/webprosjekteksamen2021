import React, {Component} from 'react';
import manImg from "../../../../assets/images/man.svg"
import womanImg from "../../../../assets/images/woman.svg"

class mainContent extends Component {
    render(){
        return(
            <div className={"mainContent"}>
                <div className={"mainContent__header--wrapper"}>
                    <div className={"mainContent__header"}>
                        <h2 className={"firstLine_header_content"}>
                            Velkommen til Gyldne Pizza
                        </h2>
                        <h2 className={"secondLine_header_content"}>
                            Happy has five letters
                        </h2>
                        <h2 className={"thirdLine_header_content"}>
                            Pizza has five letters, this is no coincidence
                        </h2>
                    </div>
                </div>

                <div className={"imgContent"}>
                    <div className={"manImg"}>
                    <img
                        src={manImg}
                        alt={"man waitress"}
                    />
                    </div>
                    <div className={"womanImg"}>
                    <img
                        src={womanImg}
                        alt={"woman waitress"}
                    />
                    </div>
                </div>

            </div>
        ); 
    }
}

export default mainContent;