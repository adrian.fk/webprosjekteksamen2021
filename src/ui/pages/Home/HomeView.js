import React from 'react';
import Page from "../../component/Page";
import HomePageNav from "./Widget/HomePageNav";
import AdminButton from "./Widget/AdminButton";
import BackgroundImage from "./Widget/BackgroundImage";
import Content from "./Widget/Content";
import "../../style/Home.css";

class HomeView extends Page {
    render() {
        return (
            <div className={"home"}>
                <HomePageNav/>
                <AdminButton/>
                <Content/>
                <BackgroundImage/>

            </div>
        );
    }
}

export default HomeView;