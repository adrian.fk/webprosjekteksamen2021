import React from 'react';
import CheckoutController from '../../../infrastructure/controllers/CheckoutController';
import Page from "../../component/Page";
import HeaderView from "../../component/Header/HeaderView";
import HeroSection from "../../component/HeroSection/HeroSection";
import EditCheckoutWidget from "./Widget/EditCheckoutWidget";
import CurrentCartWidget from "./Widget/CurrentCartWidget";
import CheckoutsLogWidget from "./Widget/CheckoutsLogWidget";
import NoteRoundedIcon from '@material-ui/icons/NoteRounded';

import '../../style/Checkout.css'
import Strings from '../../Strings/Strings';

class CheckoutsView extends Page {

    constructor(props, context) {
        super(props, context);

        this.id = "Checkout"
        this.className = "checkouts"

        this.state = {
            checkouts: [],
            displayEditRestaurantId: false,
        }
    }
    componentDidMount() {
        CheckoutController().init(this);
        this._configHeroSection();
    }

    componentDidUpdate() {
        this._configHeroSection();
    }
    
    _configHeroSection() {
        HeroSection.setTitle(Strings.getCheckoutHeroTitle());
        HeroSection.clearBtns();
        HeroSection.addButton({
            id: "1",
            text: Strings.getCheckoutHeroShoppingcart(),
            linkTo: "#",
            onClickHandler: (e) => {
                document.getElementById("CheckoutCurCartWidget")?.scrollIntoView({
                    behaviour: "smooth"
                });
            }
        });
        HeroSection.addButton({
            id: "2",
            text: Strings.getCheckoutHeroAllOrders(),
            linkTo: "#",
            onClickHandler: (e) => {
                document.getElementById("CheckoutsLogWidget")?.scrollIntoView({
                    behavior: "smooth"
                });
            }
        });
        HeroSection.addButton({
            id: "3",
            text: Strings.getCheckoutHeroMenu(),
            linkTo: "/menu"
        });
    }


    displayEditCheckout() {
        this.setState({displayEditRestaurantId: !this.state.displayEditRestaurantId});
    }

    displayCheckout(checkouts = [{restaurantId: "", menuEntries: []}]) {
        console.log(checkouts)
        this.setState({checkouts: checkouts});
    }

    rerender() {
        CheckoutController().fetchCheckoutLog();
    }

    render() {
        return (
            <section className={this.className}>
                <HeaderView activePageName={HeaderView.ACTIVE_PAGE_CHECKOUT} />
                <HeroSection
                    title={"Checkout"}
                    imgUrl={"https://images4.alphacoders.com/107/thumb-1920-1074058.jpg"}
                />
                <CurrentCartWidget />
                <section className={"checkouts__logTitle"}>
                    <NoteRoundedIcon className={this.className + "__titleIcon"} />
                    <h3>{Strings.getCheckoutCartMainTitle()}</h3>
                </section>
                <CheckoutsLogWidget />
                {this.state.displayEditRestaurantId ? <EditCheckoutWidget /> : ""}
            </section>
        );
    }
}

export default CheckoutsView;