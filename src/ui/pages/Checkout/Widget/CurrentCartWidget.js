import { Component } from "react";
import CheckoutController from "../../../../infrastructure/controllers/CheckoutController";
import RemoveCircleIcon from '@material-ui/icons/RemoveCircle';
import Strings from "../../../Strings/Strings";


/**
 * @Objective: Display the current shopping cart
 */

class CurrentCartWidget extends Component {

    constructor(props, context) {
        super(props, context);

        this.id = "CheckoutCurCartWidget"
        this.classname = "checkoutCurCartWidget"

        this.state = {
            restaurant: {},
            cart: []
        }
    }

    componentDidMount() {
        CheckoutController().initCheckoutsCartViewWidget(this);
    }

    clearCart() {
        this.setState({
            restaurant: {},
            cart: []
        })
    }

    displayCart(restaurant = {name: "", address: ""}, cart = []) {
        this.clearCart();
        this.setState({
            restaurant,
            cart
        })
        console.log(restaurant)
    }

    onSubmitCartClicked(e) {
        CheckoutController().onSubmitCart();
    }

    onRemoveBtnClicked(e, foodId) {
        CheckoutController().onRemoveShoppingCartItem(foodId);

    }

    onEditRestaurantClicked(e) {
        CheckoutController().onEditInit();
    }

    render() {
        return(
            <section
                id={this.id}
                className={this.classname + ""}
                onClick={(e) => {}}
            >
                {this.state.cart.length > 0 ?

                    <div className={this.classname + "__content"}>
                        <h2>{Strings.getCheckoutShoppingcartTitle()}</h2>
                        <h3>Restaurant: {this.state.restaurant.name}</h3>
                        {this.state.cart.map(
                            (menuEntry) =>
                                <div className={this.classname + "__menuEntry--wrapper"}>
                                    <div className={this.classname + "__menuEntry"}>
                                        <h5>{menuEntry.dishName}</h5>
                                        <p>{menuEntry.price} kr</p>
                                    </div>
                                    <div
                                        className={this.classname + "__menuEntryIcon"}
                                        onClick={(e) => this.onRemoveBtnClicked(e, menuEntry.foodId)}
                                    >
                                        <RemoveCircleIcon />
                                    </div>
                                </div>
                        )}
                        <button
                            className={"button is-fullwidth is-large is-success is-rounded"}
                            onClick={(e) => this.onSubmitCartClicked(e)}
                        >
                            {Strings.getCheckoutShoppingcartConfirmBtn()}
                        </button>
                        <button
                            className={this.classname + "__editRestaurant button is-fullwidth is-large is-link is-rounded"}
                            onClick={(e) => this.onEditRestaurantClicked(e)}
                        >
                            {Strings.getCheckoutShoppingcartChangeRestaurantBtn()}
                        </button>

                    </div>

                :
                    <div>
                        <h5>{Strings.getChekcoutEmptyCartTitle()}</h5>
                        <p>{Strings.getChekcoutEmptyCartParagraph()}</p>
                    </div>
                }
            </section>

        );
    }
}

export default CurrentCartWidget;