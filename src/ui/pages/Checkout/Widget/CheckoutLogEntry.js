import React, {Component} from 'react';
import Strings from '../../../Strings/Strings';

class CheckoutLogEntry extends Component {

    constructor(props = {restaurantName: "", menuEntries: []}, context) {
        super(props, context);
        this.className = "checkoutLogEntry"

        this.state = {
            restaurantName: props.restaurantName
        }
    }

    render() {
        return (
            <div className={this.className + " column is-3"} >
                <div className="card" onClick={e => e.stopPropagation()}>
                    <div className="card-content">
                        <div className="media">
                            <div className="media-content">
                                <h3>{this.state.restaurantName}</h3>
                                <label className={"label"}>{Strings.getCheckoutCartLogTitle()}</label>
                                {this.props.menuEntries.map(
                                    (menuEntry) => {
                                        return <div>
                                            <h5>{menuEntry.dishName}</h5>
                                            <h6>{menuEntry.price}</h6>
                                        </div>
                                       }
                                )}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default CheckoutLogEntry;