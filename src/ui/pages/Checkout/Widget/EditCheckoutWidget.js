import React, {Component} from 'react';
import CheckoutController from "../../../../infrastructure/controllers/CheckoutController";

class EditCheckoutWidget extends Component {

    constructor(props, context) {
        super(props, context);
        this.id = "EditCheckoutWidget";
        this.className = "editRestaurantWidget";

        this.state = {
            restaurantId: ""
        }
    }

    _onRestaurantIdChanged(e) {
        this.setState({
            restaurantId: e.target.value
        })
    }

    _onSubmit(e) {
        CheckoutController().onSubmitRestaurantId(this.state.restaurantId);
    }

    _exitEditCheckout() {
        CheckoutController().onExitEditCheckout();
    }

    render() {
        return (
            <div className={this.className + "__wrapper"}>
                <div className={this.className + "__background"} onClick={() => this._exitEditCheckout()}>
                    <div className={this.className + "__container"}>
                        <div className="card" onClick={e => e.stopPropagation()}>
                            <div className="card-content">
                                <div className={this.className + "__inputFields m-5"}>
                                    <label>Restaurant ID</label>
                                    <input
                                        className="input is-4 is-rounded"
                                        placeholder={"Restaurant ID"}
                                        value={this.state.restaurantId}
                                        onChange={(e) => this._onRestaurantIdChanged(e)}
                                    />
                                </div>
                                <div className="media m-5">
                                    <button
                                        className={"button is-medium is-fullwidth is-black is-rounded"}
                                        type={"submit"}
                                        onClick={(e) => this._onSubmit(e)}
                                    >
                                        SUBMIT
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EditCheckoutWidget;