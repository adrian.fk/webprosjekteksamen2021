import React, {Component} from 'react';
import CheckoutController from "../../../../infrastructure/controllers/CheckoutController";
import CheckoutLogEntry from "./CheckoutLogEntry";

/**
 * @Objective: Show the previous checkouts
 */

class CheckoutsLogWidget extends Component {

    constructor(props, context) {
        super(props, context);
        this.id = "CheckoutsLogWidget";
        this.className = "checkoutsLogWidget";

        this.state = {
            checkouts: []
        }
    }

    componentDidMount() {
        CheckoutController().initCheckoutsLogViewWidget(this);
    }

    _clearCheckoutsState() {
        this.setState({checkouts: []})
    }

    displayCheckoutLog(checkouts = []) {
        this._clearCheckoutsState();
        this.setState({checkouts});
    }

    render() {
        return (
            <div id={this.id} className={this.className + " columns is-multiline has-12"}>
                {this.state.checkouts.length > 0 ?
                    this.state.checkouts.map(
                        (checkout) => {
                            return <CheckoutLogEntry restaurantName={checkout.restaurant?.name} menuEntries={checkout.menuEntries}/>
                        }
                    )
                    :
                    ""
                }
            </div>
        );
    }
}

export default CheckoutsLogWidget;