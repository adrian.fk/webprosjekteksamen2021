import React, {Component} from 'react';
import EmployeesController from "../../../../infrastructure/controllers/EmployeesController";
import Strings from "../../../Strings/Strings";

class EditEmployeeView extends Component {

    constructor(props = {id: ""}, context) {
        super(props, context);

        this.id = "EditEmployeeView";
        this.className = "editEmployee"

        if (props.id === "") {
            console.error(this.id + " attempted to be instantiated without a proprietary ID")
        }

        this.state = {
            employeeNumber: "" + props.id,
            name: "",
            age: "",
            email: "",
            tlfnr: "",
            imgUrl: "",
            salary: "",
            title: ""
        }
    }

    
    componentDidMount() {
        EmployeesController().initEditEmployee(this.state.employeeNumber, this)
    }

    updateViewState(id, name, age, email, tlfnr, imgUrl, salary, title) {
        this.setState({
            id,
            name,
            age,
            email,
            tlfnr,
            imgUrl,
            salary,
            title
        })
    }

    onExitEditEmployeeView() {
        EmployeesController().onExitEditEmployeeView();
    }

    onInputImgUrlChanged(e) {
        this.setState({
            ...this.state,
            imgUrl: e.target.value
        });
    }

    onInputNameChanged(e) {
        this.setState({
            name: e.target.value
        });
    }

    onInputAgeChanged(e) {
        this.setState({
            age: e.target.value
        });
    }

    onInputEmailChanged(e) {
        this.setState({
            email: e.target.value
        });
    }

    onInputTlfnrChanged(e) {
        this.setState({
            tlfnr: e.target.value
        });
    }

    onInputSalaryChanged(e) {
        this.setState({
            salary: e.target.value
        });
    }

    onInputTitleChanged(e) {
        this.setState({
            title: e.target.value
        });
    }

    updateBtnClicked(e) {
        e.preventDefault();

        EmployeesController().onUpdateEmployee(
            this.state.employeeNumber,
            this.state.name,
            this.state.age,
            this.state.email,
            this.state.tlfnr,
            this.state.imgUrl,
            this.state.salary,
            this.state.title
        )
    }


    removeEmployeeClicked(e) {
        e.preventDefault();

        EmployeesController().onRemoveEmployee(this.state.employeeNumber)
    }

    render() {
        return (
            <div id={this.id} className={this.className + "--wrapper"} >
                <div
                    id={this.id}
                    className={this.className + "__background"}
                    onClick={() => this.onExitEditEmployeeView()}
                >

                    <form className={this.className}
                          onSubmit={(e) => this.updateBtnClicked(e)}
                    >
                        <div className="card" onClick={e => e.stopPropagation()}>
                            <div className="card-image">
                                <figure className="image is-4by3">
                                    <img src={this.state.imgUrl} alt="" />
                                </figure>
                            </div>
                            <div className="card-content">
                                <div className="media">
                                    <div className="media-content">
                                        <label>{Strings.EMPLOYEES_EDIT_IMG_URL()}</label>
                                        <input
                                            className="input is-4 is-rounded"
                                            placeholder={Strings.EMPLOYEES_EDIT_IMG_URL()}
                                            value={this.state.imgUrl}
                                            onChange={(e) => this.onInputImgUrlChanged(e)}
                                        />
                                        <label>{Strings.EMPLOYEES_EDIT_NAME()}</label>
                                        <input
                                            className="input is-4 is-rounded"
                                            placeholder={Strings.EMPLOYEES_EDIT_NAME()}
                                            value={this.state.name}
                                            onChange={e => this.onInputNameChanged(e)}
                                        />
                                        <label>{Strings.EMPLOYEES_EDIT_POSITION()}</label>
                                        <input
                                            className="input is-6 is-rounded"
                                            placeholder={Strings.EMPLOYEES_EDIT_POSITION()}
                                            value={this.state.title}
                                            onChange={(e) => this.onInputTitleChanged(e)}
                                        />
                                    </div>
                                </div>

                                <div className="content">
                                    <label>{Strings.EMPLOYEES_EDIT_AGE()}</label>
                                    <input
                                        className={"input is-rounded"}
                                        placeholder={Strings.EMPLOYEES_EDIT_AGE()}
                                        value={this.state.age}
                                        onChange={e => this.onInputAgeChanged(e)}
                                    />
                                    <label>{Strings.EMPLOYEES_EDIT_SALARY()}</label>
                                    <input
                                        className={"input is-rounded"}
                                        placeholder={Strings.EMPLOYEES_EDIT_SALARY()}
                                        value={this.state.salary}
                                        onChange={e => this.onInputSalaryChanged(e)}
                                    />
                                    <label>{Strings.EMPLOYEES_EDIT_PHONE()}</label>
                                    <input
                                        className={"input is-rounded"}
                                        placeholder={Strings.EMPLOYEES_EDIT_PHONE()}
                                        value={this.state.tlfnr}
                                        onChange={e => this.onInputTlfnrChanged(e)}
                                    />
                                    <label>{Strings.EMPLOYEES_EDIT_EMAIL()}</label>
                                    <input
                                        className={"input is-rounded"}
                                        placeholder={Strings.EMPLOYEES_EDIT_EMAIL()}
                                        value={this.state.email}
                                        onChange={e => this.onInputEmailChanged(e)}
                                    />

                                    <button
                                        className={"button is-medium is-fullwidth is-black is-rounded"}
                                        type={"submit"}
                                    >
                                        {Strings.EMPLOYEES_EDIT_BTN_SUBMIT()}
                                    </button>

                                    <button
                                        className={"button is-medium is-fullwidth is-active is-rounded"}
                                        onClick={(e) => this.removeEmployeeClicked(e)}
                                    >
                                        {Strings.EMPLOYEES_EDIT_BTN_REMOVE()}
                                    </button>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        )
    }
}

export default EditEmployeeView;