import React, { Component } from "react";
import EmployeesController from "../../../../infrastructure/controllers/EmployeesController";
import Strings from "../../../Strings/Strings";

class EmployeeView extends Component {

    constructor(props, context) {
        super(props, context);

        this.className = "employeeView"

        this.state = {
            id: props.id,
            name: props.name,
            age: props.age,
            email: props.email,
            tlfnr: props.tlfnr,
            imgUrl: props.imgUrl,
            salary: props.salary,
            title: props.title
        }
    }

    render() {
        return (
            <div
                className={this.className + " column is-3"}
                onClick={(e) => EmployeesController().onEmployeeClicked(this.state.id)}
            >
                <div className="card">
                    <div className="card-image">
                        <figure className="image is-4by3">
                            <img src={this.state.imgUrl} alt="" />
                        </figure>
                    </div>
                    <div className="card-content">
                        <div className="media">
                            <div className="media-content">
                                <p className="title is-4">{this.state.name}</p>
                                <p className="subtitle is-6">{this.state.title}</p>
                            </div>
                        </div>

                        <div className="content">
                            <label>{Strings.getEmployeesCardAge()}</label>
                            <p className="title is-6">{this.state.age}</p>
                            <label>{Strings.getEmployeesCardSalary()}</label>
                            <p className="title is-6">{this.state.salary}</p>
                            <label>{Strings.getEmployeesCardPhone()}</label>
                            <p className="title is-6">{this.state.tlfnr}</p>
                            <label>{Strings.getEmployeesCardEmail()}</label>
                            <p className="title is-6">{this.state.email}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EmployeeView;