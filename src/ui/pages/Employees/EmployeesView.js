import React from 'react';
import Page from "../../component/Page";
import HeaderView from "../../component/Header/HeaderView";
import HeroSection from "../../component/HeroSection/HeroSection";
import EmployeeView from "./Widget/EmployeeView";
import EmployeesController from "../../../infrastructure/controllers/EmployeesController";
import EditEmployeeView from "./Widget/EditEmployeeView";
import PeopleAltRoundedIcon from '@material-ui/icons/PeopleAltRounded';
import Strings from '../../Strings/Strings';


import '../../style/Employees.css'

class EmployeesView extends Page {

    constructor(props, context) {
        super(props, context);

        this.id = "Employees"
        this.className = "employees"


        this.state = {
            employees: [],
            displayEditEmployeeView: false,
            displayEditEmployeeViewId: ""
        }
    }

    componentDidMount() {
        EmployeesController().init(this);
    }
    
    componentDidUpdate() {
        HeroSection.setTitle(Strings.getEmployeesHeroTitle())
        HeroSection.clearBtns();
        HeroSection.addButton({
            id: "1",
            text: Strings.getEmployeesHeroOverview(),
            linkTo: "#",
            onClickHandler: (e) => {
                document.getElementById(this.id)?.scrollIntoView({
                    behavior: "smooth"
                });
            }
        });
        HeroSection.addButton({
            id: "2",
            text: Strings.getEmployeesSeStatistics(),
            linkTo: "/statistics"
        });
    
        HeroSection.addButton({
            id: "3",
            text: Strings.getEmployeesHeroAddEmployee(),
            linkTo: "#",
            onClickHandler: (e) => {
                this.addBtnClicked();
            }
        })
    }

    clearEmployees() {
        this.setState({
            employees: []
        })
    }

    displayEmployees(employees = [{id: "", name: "", age: "", email: "", tlfnr: "", imgUrl: "", salary: "", title: ""}]) {
        this.clearEmployees();
        this.setState({employees});
    }

    displayEditEmployeeView(id) {
        this.setState({
            displayEditEmployeeViewId: id
        })
        this.setState({
            displayEditEmployeeView: true
        })
    }

    hideEditEmployeeView() {
        this.setState({
            displayEditEmployeeView: false,
            displayEditEmployeeViewId: ""
        })
    }

    rerender() {
        EmployeesController().fetchEmployees();
    }

    addBtnClicked() {
        EmployeesController().onAddEmployee("", "", "", "", "", "", "", "");
    }

    render() {
        return (
            <section className={this.className}>
                <HeaderView activePageName={HeaderView.ACTIVE_PAGE_EMPLOYEES} />
                <HeroSection
                    title={"Våre ansatte"}
                    imgUrl={"https://www.visitoslo.com/contentassets/9a10dede31ab4a38b56a9987fd423269/listen-to-baljit.jpg"}
                />
                <section id={this.id} className={this.className + "__container"}>
                <div className={this.className + "__title"}>
                        <PeopleAltRoundedIcon className={this.className + "__titleIcon"} />
                        <h2>{Strings.getEmployeesOverviewTitle()}</h2>
                    </div>
                    {
                        this.state.employees.length > 0 ?
                            <section className={this.className + " columns is-multiline has-12"}>
                                {this.state.employees.map(
                                (employee) =>
                                    <EmployeeView
                                        id={employee.id}
                                        name={employee.name}
                                        age={employee.age}
                                        email={employee.email}
                                        tlfnr={employee.tlfnr}
                                        imgUrl={employee.imgUrl}
                                        salary={employee.salary}
                                        title={employee.title}
                                    />
                                )}
                            </section>
                        :
                            <div className={this.className + "__noContent"}>
                                <p>{Strings.EMPLOYEES_OVERVIEW_NO_EMPLOYEE()}</p>
                            </div>
                    }
                </section>
                { this.state.displayEditEmployeeView ? <EditEmployeeView id={this.state.displayEditEmployeeViewId} /> : ""}
            </section>
        );
    }
}

export default EmployeesView;