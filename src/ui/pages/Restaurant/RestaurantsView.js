import React from 'react';
import Page from "../../component/Page";
import HeaderView from "../../component/Header/HeaderView";
import HeroSection from "../../component/HeroSection/HeroSection";
import RestaurantView from "./Widget/RestaurantView";
import RestaurantsController from "../../../infrastructure/controllers/RestaurantsController";
import EditRestaurantView from "./Widget/EditRestaurantView";
import RestaurantIcon from '@material-ui/icons/Restaurant';



import '../../style/Restaurants.css'
import Strings from '../../Strings/Strings';

class RestaurantsView extends Page {

    constructor(props, context) {
        super(props, context);

        this.id = "RestaurantsView"
        this.className = "restaurants"


        this.state = {
            restaurants: [],
            displayEditRestaurantView: false,
            displayEditRestaurantViewId: "",
            heroSectionTitle: ""
        }
    }

    componentDidMount() {
        RestaurantsController().init(this);
    }
    
    componentDidUpdate() {
        HeroSection.setTitle(Strings.getRestaurantsHeroTitle());
        HeroSection.clearBtns();
        HeroSection.addButton({
            id: "1",
            text: Strings.getRestaurantsHeroOverview(),
            linkTo: "#",
            onClickHandler: (e) => {
                document.getElementById(this.id)?.scrollIntoView({
                    behaviour: "smooth"
                });
            }
        });
        HeroSection.addButton({
            id: "2",
            text: Strings.getRestaurantsHeroAddOrder(),
            linkTo: "/menu"
        });
        HeroSection.addButton({
            id: "3",
            text: Strings.getRestaurantsHeroAddRestaurants(),
            linkTo: "#",
            onClickHandler: (e) => {
                this.addBtnClicked();
            }
        })
    }


    clearRestaurants() {
        this.setState({
            restaurants: []
        })
    }

    displayRestaurants(restaurants = [{id: "", name: "", imgUrl: "", tlfnr: "", address: "", managerName: ""}]) {
        this.clearRestaurants();
        this.setState({restaurants});
    }

    displayEditRestaurantView(id) {
        this.setState({
            displayEditRestaurantViewId: id
        })
        this.setState({
            displayEditRestaurantView: true
        })
    }

    hideEditRestaurantView() {
        this.setState({
            displayEditRestaurantView: false,
            displayEditRestaurantViewId: ""
        })
    }

    rerender() {
        RestaurantsController().fetchRestaurants();
    }


    addBtnClicked() {
        RestaurantsController().onAddRestaurant("", "", "", "", "");
    }

    render() {
        return (
            <section className={this.className}>
                <HeaderView activePageName={HeaderView.ACTIVE_PAGE_RESTAURANTS} />
                <HeroSection
                    imgUrl={"https://images6.alphacoders.com/609/thumb-1920-609345.jpg"}
                />

                <section id={this.id} className={this.className + "__container"}>
                    <div className={this.className + "__title"}>
                        <RestaurantIcon
                            className={this.className + "__restaurantIcon"}
                        />
                        <h2>{Strings.getRestaurantsOverviewTitle()}</h2>
                    </div>
                    {
                        this.state.restaurants.length > 0 ?
                            <section className={this.className + " columns is-multiline has-12"}>
                                {this.state.restaurants.map(
                                    (restaurant) =>
                                        <RestaurantView
                                            id={restaurant.id}
                                            name={restaurant.name}
                                            imgUrl={restaurant.imgUrl}
                                            tlfnr={restaurant.tlfnr}
                                            address={restaurant.address}
                                            managerName={restaurant.managerName}
                                        />
                                )}
                            </section>
                        :
                            <div className={this.className + "__noContent"}>
                                <p>{Strings.RESTAURANTS_OVERVIEW_NO_CONTENT()}</p>
                            </div>
                    }

                </section>
                { this.state.displayEditRestaurantView ? <EditRestaurantView id={this.state.displayEditRestaurantViewId} /> : ""}
            </section>
        );
    }
}

export default RestaurantsView;