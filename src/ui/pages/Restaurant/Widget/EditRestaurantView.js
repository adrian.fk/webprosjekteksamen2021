import React, {Component} from 'react';
import RestaurantsController from "../../../../infrastructure/controllers/RestaurantsController";
import Strings from "../../../Strings/Strings";

class EditRestaurantView extends Component {

    constructor(props = {id: ""}, context) {
        super(props, context);

        this.id = "EditRestaurantView";
        this.className = "editRestaurant"

        if (props.id === "") {
            console.error(this.id + " attempted to be instantiated without a proprietary ID")
        }

        this.state = {
            restaurantId: "" + props.id,
            name: "",
            imgUrl: "",
            tlfnr: "",
            address: "",
            managerEmployeeNumber: ""
        }
    }


    componentDidMount() {
        RestaurantsController().initEditRestaurant(this.state.restaurantId, this)
    }

    updateViewState(id, name, imgUrl, tlfnr, address, managerEmployeeNum) {
        this.setState({
            id,
            name,
            imgUrl,
            tlfnr,
            address,
            managerEmployeeNum
        })
    }

    onExitEditRestaurantView() {
        RestaurantsController().onExitEditRestaurantView();
    }

    onInputImgUrlChanged(e) {
        this.setState({
            ...this.state,
            imgUrl: e.target.value
        });
    }

    onInputNameChanged(e) {
        this.setState({
            name: e.target.value
        });
    }

    onInputAddressChanged(e) {
        this.setState({
            address: e.target.value
        });
    }

    onInputTlfnrChanged(e) {
        this.setState({
            tlfnr: e.target.value
        });
    }

    onInputManagerNumChanged(e) {
        this.setState({
            managerEmployeeNum: e.target.value
        });
    }


    updateBtnClicked(e) {
        e.preventDefault();

        RestaurantsController().onUpdateRestaurant(
            this.state.restaurantId,
            this.state.name,
            this.state.imgUrl,
            this.state.tlfnr,
            this.state.address,
            this.state.managerEmployeeNum
        )
    }


    removeRestaurantClicked(e) {
        e.preventDefault();

        RestaurantsController().onRemoveRestaurant(this.state.restaurantId)
    }

    render() {
        return (
            <div className={this.className + "--wrapper"} >
                <div
                    className={this.className + "__background"}
                    onClick={() => this.onExitEditRestaurantView()}
                >

                    <form id={this.id} className={this.className}
                          onSubmit={(e) => this.updateBtnClicked(e)}
                    >
                        <div className="card" onClick={e => e.stopPropagation()}>
                            <div className="card-image">
                                <figure className="image is-4by3">
                                    <img src={this.state.imgUrl} alt="image" />
                                </figure>
                            </div>
                            <div className="card-content">
                                <div className="media">
                                    <div className="media-content">
                                        <label>ID</label>
                                        <h5>{this.state.restaurantId}</h5>
                                        <label>{Strings.RESTAURANTS_EDIT_IMG_URL()}</label>
                                        <input
                                            className="input is-4 is-rounded"
                                            placeholder={Strings.RESTAURANTS_EDIT_IMG_URL()}
                                            value={this.state.imgUrl}
                                            onChange={(e) => this.onInputImgUrlChanged(e)}
                                        />
                                        <label>{Strings.RESTAURANTS_EDIT_NAME()}</label>
                                        <input
                                            className="input is-4 is-rounded"
                                            placeholder={Strings.RESTAURANTS_EDIT_NAME()}
                                            value={this.state.name}
                                            onChange={e => this.onInputNameChanged(e)}
                                        />
                                        <label>{Strings.RESTAURANTS_EDIT_ADDRESS()}</label>
                                        <input
                                            className="input is-6 is-rounded"
                                            placeholder={Strings.RESTAURANTS_EDIT_ADDRESS()}
                                            value={this.state.address}
                                            onChange={(e) => this.onInputAddressChanged(e)}
                                        />
                                    </div>
                                </div>

                                <div className="content">
                                    <label>{Strings.RESTAURANTS_EDIT_PHONE()}</label>
                                    <input
                                        className={"input is-rounded"}
                                        placeholder={Strings.RESTAURANTS_EDIT_PHONE()}
                                        value={this.state.tlfnr}
                                        onChange={e => this.onInputTlfnrChanged(e)}
                                    />
                                    <label>{Strings.RESTAURANTS_EDIT_CEO()}</label>
                                    <input
                                        className={"input is-rounded"}
                                        placeholder={Strings.RESTAURANTS_EDIT_CEO()}
                                        value={this.state.managerEmployeeNum}
                                        onChange={e => this.onInputManagerNumChanged(e)}
                                    />

                                    <button
                                        className={"button is-medium is-fullwidth is-black is-rounded"}
                                        type={"submit"}
                                    >
                                        {Strings.RESTAURANTS_EDIT_BTN_SUBMIT()}
                                    </button>

                                    <button
                                        className={"button is-medium is-fullwidth is-active is-rounded"}
                                        onClick={(e) => this.removeRestaurantClicked(e)}
                                    >
                                        {Strings.RESTAURANTS_EDIT_BTN_REMOVE()}
                                    </button>

                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        );
    }
}

export default EditRestaurantView;