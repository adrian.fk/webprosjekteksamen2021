import React, {Component} from 'react';
import RestaurantsController from "../../../../infrastructure/controllers/RestaurantsController";
import Strings from '../../../Strings/Strings';


class RestaurantView extends Component {

    constructor(props, context) {
        super(props, context);

        this.className = "restaurantView"

        this.state = {
            id: props.id,
            name: props.name,
            imgUrl: props.imgUrl,
            tlfnr: props.tlfnr,
            address: props.address,
            managerName: props.managerName
        }
    }

    render() {
        return (
            <div
                className={this.className + " column is-3"}
                onClick={(e) => RestaurantsController().onRestaurantClicked(this.state.id)}
            >
                <div className="card">
                    <div className="card-image">
                        <figure className="image is-4by3">
                            <img src={this.state.imgUrl} alt="image" />
                        </figure>
                    </div>
                    <div className="card-content">
                        <div className="media">
                            <div className="media-content">
                                <p className="title is-4">{this.state.name}</p>
                                <label>{Strings.getRestaurantsCardAdress()}</label>
                                <p className="title is-6">{this.state.address}</p>
                            </div>
                        </div>

                        <div className="content">
                            <label>{Strings.getRestaurantsCardPhone()}</label>
                            <p className={"title is-6"}>{this.state.tlfnr}</p>

                            <label>{Strings.getRestaurantsCardCeo()}</label>
                            <p className={"title is-6"}>{this.state.managerName}</p>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default RestaurantView;