import React, {Component} from 'react';
import StatisticsController from "../../../../infrastructure/controllers/StatisticsController";
import AttachMoneyRoundedIcon from '@material-ui/icons/AttachMoneyRounded';
import Strings from "../../../Strings/Strings";

class EconomicsWidget extends Component {

    constructor(props, context) {
        super(props, context);
        this.id = "StatisticsEconomicsViewWidget"
        this.className = "statisticsEconomicsViewWidget"

        this.state = {
            totalRevenue: "",
            totalSalary: "",
        }
    }

    componentDidMount() {
        StatisticsController().initEconomics(this);
    }

    clearViewState() {
        this.setState({
            totalRevenue: "",
            totalSalary: "",
        });
    }

    displayEconomics(totalRevenue = "", totalSalary = "") {
        this.clearViewState();
        this.setState({
            totalRevenue,
            totalSalary
        });
    }

    render() {
        return (
            <div id={this.id} className={this.className} >
                <div className={this.className + "__title"}>
                    <AttachMoneyRoundedIcon className={"statistics__titleIcon"} />
                    <h3>{Strings.STATISTICS_ECONOMICS_TITLE()}</h3>
                </div>
                <div className={this.className + "__container"}>
                    <div className={this.className + "__leftSection"}>
                        <div className={this.className + "__revenue " + this.className + "__infoSection"}>
                            <h3>{Strings.STATISTICS_ECONOMICS_TOTAL_REVENUE()}</h3>
                            <h4>{this.state.totalRevenue}</h4>
                        </div>
                    </div>
                    <div className={this.className + "__rightSection"}>
                        <div className={this.className + "__salary " + this.className + "__infoSection"}>
                            <h3>{Strings.STATISTICS_ECONOMICS_TOTAL_SALARIES()}</h3>
                            <h4>{this.state.totalSalary}</h4>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default EconomicsWidget;