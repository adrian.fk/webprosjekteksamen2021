import React, {Component} from 'react';
import StatisticsController from "../../../../infrastructure/controllers/StatisticsController";
import FileCopyRoundedIcon from '@material-ui/icons/FileCopyRounded';
import Strings from "../../../Strings/Strings";

class RestaurantWidget extends Component {

    constructor(props, context) {
        super(props, context);
        this.id = "StatisticsRestaurantViewWidget"
        this.className = "statisticsRestaurantViewWidget"

        this.state = {
            restaurants: [],
        }
    }

    componentDidMount() {
        StatisticsController().initRestaurants(this);
    }

    clearViewState() {
        this.setState({
            restaurants: [],
        });
    }

    displayRestaurants(restaurants) {
        console.log(restaurants);
        this.clearViewState();
        this.setState({
            restaurants
        });
    }

    render() {
        return (
            <div id={this.id} className={this.className} >
                <div className={this.className + "__title"}>
                    <FileCopyRoundedIcon className={"statistics__titleIcon"} />
                    <h3>{Strings.STATISTICS_RESTAURANT_TITLE()}</h3>
                </div>
                <div className={this.className + "__columns--wrapper"}>
                    <div className={"columns is-multiline has-12"}>

                        {this.state.restaurants?.map(
                            (restaurant) => {
                                return <div className={"column is-3"}>
                                    <label>{Strings.STATISTICS_RESTAURANT_NAME()}</label>
                                    <h3>{restaurant.name}</h3>
                                    <label>{Strings.STATISTICS_RESTAURANT_AVG_PRICE}</label>
                                    <h5>{restaurant.avgPriceSoldItem}</h5>
                                    <label>{Strings.STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_TEXT()}</label>
                                    {restaurant?.mostSoldItems?.map(mostSoldItem => <div className={this.className + "__soldUnit"}>
                                        <label>{Strings.STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_NAME()}</label>
                                        <p>{mostSoldItem.name}</p>
                                        <label>{Strings.STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_AMOUNT()}</label>
                                        <p>{mostSoldItem.soldUnits}</p>
                                    </div>)}
                                </div>
                            }
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default RestaurantWidget;