import React, {Component} from 'react';
import StatisticsController from "../../../../infrastructure/controllers/StatisticsController";
import CreditCardRoundedIcon from '@material-ui/icons/CreditCardRounded';
import Strings from "../../../Strings/Strings";

class SalesWidget extends Component {

    constructor(props, context) {
        super(props, context);
        this.id = "StatisticsSalesViewWidget"
        this.className = "statisticsSalesViewWidget"


        this.state = {
            restaurants: [],
            mostSoldItem: {},
            avgPriceSoldFor: ""
        }
    }

    componentDidMount() {
        StatisticsController().initSales(this);
    }

    clearViewState() {
        this.setState({
            restaurants: [],
            mostSoldEntry: {dishName: ""},
            avgPriceSoldFor: ""
        });
    }

    displaySalesWidget(restaurants = [], mostSoldEntry, avgPriceSoldFor) {
        this.clearViewState();
        this.setState({
            restaurants,
            mostSoldEntry,
            avgPriceSoldFor
        })
    }

    render() {
        return (
            <div id={this.id} className={this.className}>
                <div className={this.className + "__title"}>
                    <CreditCardRoundedIcon className={"statistics__titleIcon"} />
                    <h2>{Strings.STATISTICS_SALES_TITLE()}</h2>
                </div>
                <div className={this.className + "__genericStats--container"}>
                    <div className={this.className + "__genericStat--wrapper"}>
                        <div className={this.className + "__mostSoldItem " + this.className + "__genericStat"}>
                            <label>{Strings.STATISTICS_SALES_MOST_SOLD_DISH()}</label>
                            <h4>{this.state.mostSoldEntry?.dishName ? this.state.mostSoldEntry.dishName : ""}</h4>
                        </div>
                        <div className={this.className + "__avgPriceSoldFor " + this.className + "__genericStat"}>
                            <label>{Strings.STATISTICS_SALES_AVG_AMT_PR_CHECKOUT()}</label>
                            <h4>{this.state.avgPriceSoldFor}</h4>
                        </div>
                    </div>
                </div>
                <div className={this.className + "__specificStats--wrapper container"}>
                    <div className={this.className + "__specificStats--container columns is-multiline has-12"}>
                        {this.state.restaurants.map(restaurant =>
                            <div className={"restaurantStat column is-3 m-3"}>
                                <h5>{restaurant.name}</h5>
                                <p>{Strings.STATISTICS_SALES_TOTAL_REVENUE()}</p>
                                <p>{restaurant.totalSold}</p>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        );
    }
}

export default SalesWidget;
