import React from 'react';
import Page from "../../component/Page";
import RestaurantWidget from "./widgets/RestaurantWidget";
import EconomicsWidget from "./widgets/EconomicsWidget";
import SalesWidget from "./widgets/SalesWidget";
import HeaderView from "../../component/Header/HeaderView";
import HeroSection from "../../component/HeroSection/HeroSection";
import StatisticsController from "../../../infrastructure/controllers/StatisticsController";

import '../../style/Statistics.css'
import Strings from "../../Strings/Strings";

class StatisticsView extends Page {

    constructor(props, context) {
        super(props, context);
        this.className = "statisticsView"
    }


    componentDidMount() {
        StatisticsController().initStatistics(this);
        this._setupHeroSection();
    }

    componentDidUpdate() {
        this._setupHeroSection();
    }

    _setupHeroSection() {
        HeroSection.setTitle(Strings.STATISTICS_HERO_TITLE())
        HeroSection.clearBtns();
        HeroSection.addButton({
            id: "1",
            text: Strings.STATISTICS_HERO_BTN_RESTAURANTS(),
            linkTo: "#",
            onClickHandler: (e) => {
                document.getElementById("StatisticsRestaurantViewWidget")?.scrollIntoView({
                    behavior: "smooth"
                })
            }
        })

        HeroSection.addButton({
            id: "2",
            text: Strings.STATISTICS_HERO_ECONOMICS(),
            linkTo: "#",
            onClickHandler: (e) => {
                document.getElementById("StatisticsEconomicsViewWidget")?.scrollIntoView({
                    behavior: "smooth"
                })
            }
        })

        HeroSection.addButton({
            id: "3",
            text: Strings.STATISTICS_HERO_SALES(),
            linkTo: "#",
            onClickHandler: (e) => {
                document.getElementById("StatisticsSalesViewWidget")?.scrollIntoView({
                    behavior: "smooth"
                });
            }
        });
    }

    render() {
        return (
            <div className={this.className}>
                <HeaderView activePageName={HeaderView.ACTIVE_PAGE_STATISTICS} />
                <HeroSection
                    imgUrl={"https://images2.alphacoders.com/796/thumb-1920-796169.jpg"}
                />
                <RestaurantWidget />
                <EconomicsWidget />
                <SalesWidget />
            </div>
        );
    }
}

export default StatisticsView;