import React, {Component} from 'react';
import {Link} from "react-router-dom";
import Strings from '../../Strings/Strings';

class HeaderNavigation extends Component {


    constructor(props = {activePageName: ""}, context) {
        super(props, context);
        this.state = {
            activePageName: props.activePageName
        }
    }

    hasActivePageState() {
        return this.state.activePageName !== "";
    }

    componentDidMount() {
        if (this.hasActivePageState()) {
            const activePageButton = document.querySelector(".headerNavigation__" + this.state.activePageName);
            activePageButton.style.borderBottom = "#AAA 2px solid";
        }
    }

    render() {
        return (
            <nav className={"header__headerNavigation"}>
                <nav className="headerNavigation__menuItem headerNavigation__home">
                    <Link to={"/home"}>{Strings.getNavbarHome()}</Link>
                </nav>
                <nav className="headerNavigation__menuItem headerNavigation__restaurants">
                    <Link to={"/restaurants"}>{Strings.getNavbarRestaurants()}</Link>
                </nav>
                <nav className="headerNavigation__menuItem headerNavigation__employees">
                    <Link to={"/employees"}>{Strings.getNavbarEmployees()}</Link>
                </nav>
                <nav className="headerNavigation__menuItem headerNavigation__menu">
                    <Link to={"/menu"}>{Strings.getNavbarMenu()}</Link>
                </nav>
                <nav className="headerNavigation__menuItem headerNavigation__checkout">
                    <Link to={"/checkout"}>{Strings.getNavbarCheckout()}</Link>
                </nav>
                <nav className="headerNavigation__menuItem headerNavigation__statistics">
                    <Link to={"/statistics"}>{Strings.getNavbarStatistics()}</Link>
                </nav>
            </nav>
        );
    }
}

export default HeaderNavigation;