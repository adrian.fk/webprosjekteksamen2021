import React, { Component } from 'react'
import App from '../../App';
import English from '../../Strings/lang/English';
import Norweagian from '../../Strings/lang/Norweagian';
import Strings from '../../Strings/Strings';

export default class HeaderLangSelector extends Component {
    constructor(props) {
        super(props);
        this.className = "headerLangSelector";

        this.state = {
            selector: ""
        }
    }

    componentDidMount() {
        this.setState({
            selector: Strings.selector
        })
    }

    onNorweagianBtnClicked() {
        Strings.setSelector(Norweagian.LANG_NORWEAGIAN)
        this.setState({
            selector: Norweagian.LANG_NORWEAGIAN
        })
        App.forceUpdate();
    }

    onEnglishBtnClicked() {
        Strings.setSelector(English.LANG_ENGLISH)
        this.setState({
            selector: English.LANG_ENGLISH
        })
        App.forceUpdate();
    }

    render() {
        return (
            <div className={this.className}>
                <div 
                    className={this.className + "__eng"}
                    style={this.state.selector === English.LANG_ENGLISH ? {fontWeight: "bold"} : {fontWeight: "normal"}}
                    onClick={() => this.onEnglishBtnClicked()}
                >
                    <p>en</p>
                </div>
                <div 
                    className={this.className + "__saparator"}
                    style={{fontWeight: "bold"}}
                >
                    <p>|</p>
                </div>
                <div 
                    className={this.className + "__no"}
                    style={this.state.selector === Norweagian.LANG_NORWEAGIAN ? {fontWeight: "bold"} : {fontWeight: "normal"}}
                    onClick={() => this.onNorweagianBtnClicked()}
                >
                    <p>no</p>
                </div>
            </div>
        )
    }
}
