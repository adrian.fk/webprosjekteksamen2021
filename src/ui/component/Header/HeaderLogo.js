import React, {Component} from 'react';

import Logo from "../../../assets/images/logo-fixed.svg"

class HeaderLogo extends Component {
    render() {
        return (
            <div className={"header__headerLogo"}>
                <img
                    src={Logo}
                    alt={"Gyldne Pizza logo"}
                />
                <h3>Gyldne Pizza</h3>
            </div>
        );
    }
}

export default HeaderLogo;