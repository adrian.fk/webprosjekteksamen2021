import React, {Component} from 'react';
import HeaderLogo from "./HeaderLogo";
import HeaderNavigation from "./HeaderNavigation";
import HeaderLangSelector from './HeaderLangSelector';

import '../../style/Header.css'

class HeaderView extends Component {

    static ACTIVE_PAGE_HOME = Object.freeze("home");
    static ACTIVE_PAGE_RESTAURANTS = Object.freeze("restaurants");
    static ACTIVE_PAGE_EMPLOYEES = Object.freeze("employees");
    static ACTIVE_PAGE_MENU = Object.freeze("menu");
    static ACTIVE_PAGE_CHECKOUT = Object.freeze("checkout");
    static ACTIVE_PAGE_STATISTICS = Object.freeze("statistics");

    constructor(props = {activePageName: ""}, context) {
        super(props, context);
        this.state = {
            activeNavName: props.activePageName
        }
        this.registerScrollListener();
    }


    registerScrollListener() {
        window.addEventListener("scroll",
            () => {
                if (window.pageYOffset > 100) {
                    document.querySelector(".header")?.classList.add("fixedHeader");
                }
                else {
                    document.querySelector(".header")?.classList.add("fadeIn");
                    setTimeout(() => document.querySelector(".header")?.classList.remove("fadeIn"), 200)
                    document.querySelector(".header")?.classList.remove("fixedHeader")
                }
            }
        )

    }

    render() {
        return (
            <div className={"header"}>
                <HeaderLogo />
                <HeaderNavigation activePageName={this.state.activeNavName} />
                <HeaderLangSelector/>
            </div>
        );
    }
}

export default HeaderView;