import React, {Component} from 'react';
import TitleSection from "./widgets/TitleSection";
import NavSection from "./widgets/NavSection";
import ImgSection from "./widgets/ImgSection";
import HeroSectionPresenter from "./HeroSectionPresenter"

import '../../style/HeroSection.css'


/**
 *  @Objective: Servers as a React View facade for this component, as well as being the highest scoped View-group.
 *              This component is implemented as a standalone view component, hence it encapsulates it's own
 *              proprietary Presenter and Model that all results in this component being implemented with it's own local
 *              MVP architecture.
 */
class HeroSection extends Component {

    constructor(props = {title: "", imgUrl: ""}, context) {
        super(props, context);

        if (props.title === "") console.error("Hero section instantiated without title parameter")
        if (props.imgUrl === "") console.error("Hero section instantiated without imgUrl parameter")

        this.state = {
            title: props.title,
            imgUrl: props.imgUrl
        }
    }


    componentDidMount() {
        HeroSectionPresenter.bindViewInstance(this)
    }

    static addButton(button = {id: "", text: "", handler: () => {}}) {
        HeroSectionPresenter.addButton(button)
    }

    static addButtons(buttons = [{id: "", text: "", handler: () => {}}]) {
        HeroSectionPresenter.setNavButtons(buttons)
    }

    static clearBtns() {
        HeroSectionPresenter.clearBtns();
    }

    static setTitle(title) {
        HeroSectionPresenter.setTitle(title);
    }

    render() {
        return (
            <section className={"heroSection"} >
                <div className="heroSection__left">
                    < TitleSection title={this.state.title}/>
                    < NavSection />
                </div>
                <div className="heroSection__right">
                    < ImgSection imgUrl={this.state.imgUrl} />
                </div>
            </section>
        );
    }
}

export default HeroSection;