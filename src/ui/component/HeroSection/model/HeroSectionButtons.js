

export default
class HeroSectionButtons {
    constructor() {
        this.buttons = new Map();
    }

    reset() {
        this.resetButtons();
    }

    resetButtons() {
        this.buttons = new Map();
    }

    addButton(button = {id: "", text: "", linkTo: "", onClickHandler: () => {}}) {
        this.buttons.set(button.id, button);
    }

    isUseExeBtnConfig() {
        return this.buttons.length > 0;
    }

    getButtons() {
        return Array.from(this.buttons.values());
    }

    getButton(id) {
        return this.buttons.get(id);
    }
}