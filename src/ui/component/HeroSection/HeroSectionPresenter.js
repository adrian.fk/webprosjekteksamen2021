import HeroSectionButtons from "./model/HeroSectionButtons";


const HeroSectionPresenter = (() => {

    let _view;
    let _navView;
    let _titleView;

    let _buttonsModel = new HeroSectionButtons();

    const resetButtonsModel = () => {
        _buttonsModel = new HeroSectionButtons();
    }

    const bindViewInstance = (viewInstance) => {
        resetButtonsModel();
        _view = viewInstance;
    }

    const bindNavViewInstance = (viewInstance) => {
        resetButtonsModel();
        _navView = viewInstance;
    }

    const setNavButtons = (buttonsArr = []) => {
        for (const button of buttonsArr)
            addButton(button);
    }

    const addButton = (button = {id: "", text: "", linkTo: "", onClickHandler: () => {}}) => {
        _buttonsModel?.addButton(button);
        presentButtons();
    }

    const presentButtons = () => {
        _navView?.updateButtons(_buttonsModel?.getButtons());
    }

    const clearBtns = () => {
        _buttonsModel.resetButtons();
    }

    const setTitle = (title = "") => {
        _titleView.displayTitle(title);
    }

    const bindTitleViewInstance = (view) => {
        _titleView = view;
    }

    return {
        bindViewInstance,
        bindNavViewInstance,
        addButton,
        setNavButtons,
        presentButtons,
        clearBtns,
        setTitle,
        bindTitleViewInstance
    }

}) ();

export default HeroSectionPresenter;