import React, {Component} from 'react';

class ImgSection extends Component {

    constructor(props, context) {
        super(props, context);
        this.className = "ImgSection";
    }

    render() {
        return (
            <div className={this.className} >
                <img
                    src={this.props.imgUrl}
                    alt={""}
                />
            </div>
        );
    }
}

export default ImgSection;