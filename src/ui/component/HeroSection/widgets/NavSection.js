import React, {Component} from 'react';
import {Link} from "react-router-dom";
import HeroSectionPresenter from "../HeroSectionPresenter"

class NavSection extends Component {

    constructor(props, context) {
        super(props, context);
        this.className = "navSection";

        this.state = {
            buttons: []
        }
    }

    componentDidMount() {
        HeroSectionPresenter.bindNavViewInstance(this)
    }

    clearButtons() {
        this.setState({
            buttons: []
        });
    }

    updateButtons(buttons = []) {
        this.clearButtons();
        this.setState({
            buttons
        });
    }


    render() {
        return (
            <section className={this.className}>
                <nav className={this.className + "__nav"}>
                    {this.state.buttons?.length > 0 ?
                        this.state.buttons.map(
                            (button) => {
                                return <Link to={button.linkTo}>
                                    <nav className={this.className + "__btn"}
                                         onClick={(e) => {
                                             if (button.onClickHandler) button.onClickHandler(e)
                                         }}
                                    >
                                        <h5>{button.text}</h5>
                                    </nav>
                                </Link>
                            }
                        )
                        :
                        <>
                            <Link to={"/statistics"}>
                                <nav className={this.className + "__adminBtn " + this.className + "__btn"}>
                                    <h5>ADMIN</h5>
                                </nav>
                            </Link>
                            <Link to={"/menu"}>
                                <nav className={this.className + "__menuBtn " + this.className + "__btn"}>
                                    <h5>MENU</h5>
                                </nav>
                            </Link>
                            <Link to={"/checkout"}>
                                <nav className={this.className + "__checkoutBtn " + this.className + "__btn"}>
                                    <h5>CHECKOUT</h5>
                                </nav>
                            </Link>
                        </>
                    }
                </nav>
            </section>
        );
    }
}

export default NavSection;