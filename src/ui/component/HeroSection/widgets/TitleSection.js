import React, {Component} from 'react';
import Strings from '../../../Strings/Strings';
import HeroSectionPresenter from '../HeroSectionPresenter';

class TitleSection extends Component {

    constructor(props, context) {
        super(props, context);
        this.className = "titleSection";
        this.state = {
            title: props.title
        }
    }

    componentDidMount() {
        HeroSectionPresenter.bindTitleViewInstance(this)
    }

    displayTitle(title) {
        this.setState({title});
    }

    render() {
        return (
            <div className={this.className}>
                <h4>{Strings.getHeroWelcome()}</h4>
                <h3>{this.state.title}</h3>
            </div>
        );
    }
}

export default TitleSection;