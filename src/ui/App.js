import {useEffect} from "react";
import DataEmployees from "../data/DataEmployees";
import DataRestaurants from '../data/DataRestaurants';
import DataMenuEntries from '../data/DataMenuEntries';
import DataCheckouts from '../data/DataCheckouts';

import {
    BrowserRouter,
    Route,
    Switch
} from 'react-router-dom';

import Home from "./pages/Home/HomeView";
import Checkout from "./pages/Checkout/CheckoutView";
import Employees from "./pages/Employees/EmployeesView";
import Menu from "./pages/Menu/MenusView";
import Restaurant from "./pages/Restaurant/RestaurantsView";
import StatisticsView from "./pages/Statistics/StatisticsView";
import DataShoppingCart from "../data/DataShoppingCart";


import React, { Component } from 'react'

export default class App extends Component {
    static instance;



    componentDidMount() {
        App.instance = this;
    }

    static forceUpdate() {
        App.instance.forceUpdate();
        document.querySelector(".App")?.classList.add("fadeIn");
        setTimeout(() => document.querySelector(".App")?.classList.remove("fadeIn"), 300)
    }
 
    render() {
        return (
            <BrowserRouter>
            <Switch>
                <div className={"App"}>
                    < Route path={"/"} exact component={Home} />

                    < Route path={"/home"} exact component={Home} />
                    < Route path={"/checkout"} exact component={Checkout} />
                    < Route path={"/employees"} exact component={Employees} />
                    < Route path={"/menu"} exact component={Menu} />
                    < Route path={"/restaurants"} exact component={Restaurant} />
                    < Route path={"/statistics"} exact component={StatisticsView} />

                </div>
            </Switch>
        </BrowserRouter>
        )
    }
}



