
class Norweagian {
    static LANG_NORWEAGIAN = "LANG_NORWEAGIAN"
    
    static HERO_WELCOME = Object.freeze("Velkommen til")




    //Navbar
    static NAVBAR_HOME = Object.freeze("HJEM")
    static NAVBAR_RESTAURANTS = Object.freeze("RESTAURANTER")
    static NAVBAR_EMPLOYEES = Object.freeze("ANSATTE")
    static NAVBAR_MENU = Object.freeze("MENY")
    static NAVBAR_CHECKOUT = Object.freeze("KASSEN")
    static NAVBAR_STATISTICS = Object.freeze("STATISTIKK")




    //Restaurant
        //HeroSection
    static RESTAURANTS_HERO_OVERVIEW = Object.freeze("Oversikt")
    static RESTAURANTS_HERO_ADD_ORDER = Object.freeze("Legg til ordre")
    static RESTAURANTS_HERO_ADD_RESTAURANT = Object.freeze("Legg til restaurant")
    static RESTAURANTS_HERO_TITLE = Object.freeze("Våre restauranter")

    //Restaurant 
        //Restauranst section
    static RESTAURANTS_OVERVIEW_TITLE = Object.freeze("Oversikt")
    static RESTAURANTS_OVERVIEW_NO_CONTENT = Object.freeze("Ingen restaurant lagt til..")
    static RESTAURANTS_CARD_ADRESS = Object.freeze("Adresse:")
    static RESTAURANTS_CARD_PHONE = Object.freeze("Tlf. nr.:")
    static RESTAURANTS_CARD_CEO = Object.freeze("Daglig leder:")

    //Restaurant
        //Edit section
    static RESTAURANTS_EDIT_IMG_URL = Object.freeze("Bilde URL:")
    static RESTAURANTS_EDIT_NAME = Object.freeze("Navn:")
    static RESTAURANTS_EDIT_ADDRESS = Object.freeze("Adresse:")
    static RESTAURANTS_EDIT_PHONE = Object.freeze("Telefon nummer:")
    static RESTAURANTS_EDIT_CEO = Object.freeze("Daglig leder ansatt nummer:")
    static RESTAURANTS_EDIT_BTN_SUBMIT = Object.freeze("BEKREFT")
    static RESTAURANTS_EDIT_BTN_REMOVE = Object.freeze("FJERN")





    //Employees
        //HeroSection
    static EMPLOYEES_HERO_TITLE = Object.freeze("Våre ansatte");
    static EMPLOYEES_HERO_OVERVIEW = Object.freeze("Oversikt");
    static EMPLOYEES_HERO_STATISTICS = Object.freeze("Se statistikk");
    static EMPLOYEES_HERO_ADD_EMPLOYEE = Object.freeze("Legg til ansatt");

    //Employees
        //Employees section
    static EMPLOYEES_OVERVIEW_TITLE = Object.freeze("Oversikt");
    static EMPLOYEES_OVERVIEW_NO_EMPLOYEE = Object.freeze("Ingen ansatte lagt til..");
    static EMPLOYEES_CARD_AGE = Object.freeze("Alder:");
    static EMPLOYEES_CARD_SALARY = Object.freeze("Lønn:");
    static EMPLOYEES_CARD_PHONE = Object.freeze("Tlf. nr.:");
    static EMPLOYEES_CARD_EMAIL = Object.freeze("Epost:");




    //Menu
        //HeroSection
    static MENU_HERO_TITLE = Object.freeze("Vår meny");
    static MENU_HERO_OVERVIEW = Object.freeze("Oversikt");
    static MENU_HERO_SHOPPINGCART = Object.freeze("Se handlekurv");
    static MENU_HERO_ADD_DISH = Object.freeze("Legg til rett");

    //Menu
        //Menu section
    static MENU_OVERVIEW_TITLE = Object.freeze("Meny oversikt");
    static MENU_RESTAURANT_SELECTOR_TITLE = Object.freeze("Velg Restaurant");
    static MENU_CARD_PRICE = Object.freeze("Pris:");
    static MENU_CARD_DESCRIPTION = Object.freeze("Beskrivelse:");
    static MENU_CARD_ADD_TO_SHOPPINGCART = Object.freeze("LEGG I HANDLEKURVEN");
    static MENU_CARD_REMOVE_FROM_SHOPPINGCART = Object.freeze("FJERN");
    static MENU_CARD_GO_TO_SHOPPINGCART = Object.freeze("TIL KASSEN");

    //Menu
        //Edit menu entry section
    static MENU_EDIT_IMG_URL = Object.freeze("Bilde URL:");
    static MENU_EDIT_NAME = Object.freeze("Navn:");
    static MENU_EDIT_PRICE = Object.freeze("Pris:");
    static MENU_EDIT_DESCRIPTION = Object.freeze("Beskrivelse:");
    static MENU_EDIT_BTN_SUBMIT = Object.freeze("BEKREFT");
    static MENU_EDIT_BTN_REMOVE = Object.freeze("FJERN");

    //Employees
        //Employees edit section
    static EMPLOYEES_EDIT_IMG_URL = Object.freeze("Bilde URL:");
    static EMPLOYEES_EDIT_NAME = Object.freeze("Navn:");
    static EMPLOYEES_EDIT_POSITION = Object.freeze("Stilling:");
    static EMPLOYEES_EDIT_AGE = Object.freeze("Alder:");
    static EMPLOYEES_EDIT_SALARY = Object.freeze("Lønn:");
    static EMPLOYEES_EDIT_PHONE = Object.freeze("Telefon nummer:");
    static EMPLOYEES_EDIT_EMAIL = Object.freeze("Epost:");
    static EMPLOYEES_EDIT_BTN_SUBMIT = Object.freeze("BEKREFT");
    static EMPLOYEES_EDIT_BTN_REMOVE = Object.freeze("FJERN");




    //Checkout
        //HeroSection
    static CHECKOUT_HERO_TITLE = Object.freeze("Kassen");
    static CHECKOUT_HERO_ALL_ORDERS = Object.freeze("Alle ordre");
    static CHECKOUT_HERO_SHOPPINGCART = Object.freeze("Handlekurv");
    static CHECKOUT_HERO_MENU = Object.freeze("Meny");

    //Checkout
        //shoppingcart section
    static CHECKOUT_SHOPPINGCART_TITLE = Object.freeze("Handlekurv:");
    static CHECKOUT_SHOPPINGCART_CONFIRM_BTN = Object.freeze("BEKREFT");
    static CHECKOUT_SHOPPINGCART_CHANGE_RESTAURANT_BTN = Object.freeze("ENDRE RESTAURANT");
    static CHECKOUT_SHOPPINGCART_EMPTY_CART_TITLE = Object.freeze("Handlekurven er tom..");
    static CHECKOUT_SHOPPINGCART_EMPTY_CART_PARAGRAPH = Object.freeze("Besøk meny siden for å legge inn elementer i handlekurven.");

    //Checkout
        //Log section
    static CHECKOUT_CARTLOG_MAIN_TITLE = Object.freeze("Plaserte ordre");
    static CHECKOUT_CARTLOG_TITLE = Object.freeze("Handlekurv:");


    //Statistics
        //Hero section btns
    static STATISTICS_HERO_TITLE = Object.freeze("Vårt statistikk panel");
    static STATISTICS_HERO_BTN_RESTAURANTS = Object.freeze("Restauranter");
    static STATISTICS_HERO_ECONOMICS = Object.freeze("Økonomi");
    static STATISTICS_HERO_SALES = Object.freeze("Salg");

    //Statistics
        //Restaurant stats
    static STATISTICS_RESTAURANT_TITLE = Object.freeze("Restaurant statistikk")
    static STATISTICS_RESTAURANT_NAME = Object.freeze("Restaurant navn:")
    static STATISTICS_RESTAURANT_AVG_PRICE = Object.freeze("Gjennomsnitts pris:");
    static STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_TEXT = Object.freeze("Mest solgte varer:");
    static STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_NAME = Object.freeze("Navn:");
    static STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_AMOUNT = Object.freeze("Antall solgt:");

    //Statistics
        //Economics stats
    static STATISTICS_ECONOMICS_TITLE = Object.freeze("Økonomi statistikk");
    static STATISTICS_ECONOMICS_TOTAL_REVENUE = Object.freeze("Total fortjeneste:");
    static STATISTICS_ECONOMICS_TOTAL_SALARIES = Object.freeze("Total lønnsutgift:");

    //Statistics
        //Sales stats
    static STATISTICS_SALES_TITLE = Object.freeze("Salgs statistikk");
    static STATISTICS_SALES_MOST_SOLD_DISH = Object.freeze("Mest solgte rett:");
    static STATISTICS_SALES_AVG_AMT_PR_CHECKOUT = Object.freeze("Gjennomsnitts pris per ordre:");
    static STATISTICS_SALES_TOTAL_REVENUE = Object.freeze("Total fortjeneste:");

}

export default Norweagian;