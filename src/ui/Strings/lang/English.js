
class English {
    static LANG_ENGLISH = "LANG_ENGLISH"

    static HERO_WELCOME = Object.freeze("Welcome to")

    //Navbar
    static NAVBAR_HOME = Object.freeze("HOME")
    static NAVBAR_RESTAURANTS = Object.freeze("RESTAURANTS")
    static NAVBAR_EMPLOYEES = Object.freeze("EMPLOYEES")
    static NAVBAR_MENU = Object.freeze("MENU")
    static NAVBAR_CHECKOUT = Object.freeze("CHECKOUT")
    static NAVBAR_STATISTICS = Object.freeze("STATISTICS")

    //Restaurant
        //HeroSection
    static RESTAURANTS_HERO_OVERVIEW = Object.freeze("Overview")
    static RESTAURANTS_HERO_ADD_ORDER = Object.freeze("Add order")
    static RESTAURANTS_HERO_ADD_RESTAURANT = Object.freeze("Add restaurant")
    static RESTAURANTS_HERO_TITLE = Object.freeze("Our restaurants")
    
    //Restaurant 
        //Restauranst section
    static RESTAURANTS_OVERVIEW_TITLE = Object.freeze("Overview")
    static RESTAURANTS_OVERVIEW_NO_CONTENT = Object.freeze("No restaurants added..")
    static RESTAURANTS_CARD_ADDRESS = Object.freeze("Address:")
    static RESTAURANTS_CARD_PHONE = Object.freeze("Phone:")
    static RESTAURANTS_CARD_CEO = Object.freeze("CEO:")

    //Restaurant
        //Edit section
    static RESTAURANTS_EDIT_IMG_URL = Object.freeze("Image URL:")
    static RESTAURANTS_EDIT_NAME = Object.freeze("Name:")
    static RESTAURANTS_EDIT_ADDRESS = Object.freeze("Address:")
    static RESTAURANTS_EDIT_PHONE = Object.freeze("Phone:")
    static RESTAURANTS_EDIT_CEO = Object.freeze("CEO employee number:")
    static RESTAURANTS_EDIT_BTN_SUBMIT = Object.freeze("SUBMIT")
    static RESTAURANTS_EDIT_BTN_REMOVE = Object.freeze("REMOVE")

    //Employees
        //HeroSection
    static EMPLOYEES_HERO_TITLE = Object.freeze("Our employees");
    static EMPLOYEES_HERO_OVERVIEW = Object.freeze("Overview");
    static EMPLOYEES_HERO_STATISTICS = Object.freeze("See statistics");
    static EMPLOYEES_HERO_ADD_EMPLOYEE = Object.freeze("Add employee");

    //Employees
        //Employees section
    static EMPLOYEES_OVERVIEW_TITLE = Object.freeze("Overview");
    static EMPLOYEES_OVERVIEW_NO_EMPLOYEE = Object.freeze("No employees added..");
    static EMPLOYEES_CARD_AGE = Object.freeze("Age:");
    static EMPLOYEES_CARD_SALARY = Object.freeze("Salary:");
    static EMPLOYEES_CARD_PHONE = Object.freeze("Phone:");
    static EMPLOYEES_CARD_EMAIL = Object.freeze("Email:");

    //Employees
        //Employees edit section
    static EMPLOYEES_EDIT_IMG_URL = Object.freeze("Image URL:");
    static EMPLOYEES_EDIT_NAME = Object.freeze("Name:");
    static EMPLOYEES_EDIT_POSITION = Object.freeze("Position:");
    static EMPLOYEES_EDIT_AGE = Object.freeze("Age:");
    static EMPLOYEES_EDIT_SALARY = Object.freeze("Salary:");
    static EMPLOYEES_EDIT_PHONE = Object.freeze("Phone:");
    static EMPLOYEES_EDIT_EMAIL = Object.freeze("Email:");
    static EMPLOYEES_EDIT_BTN_SUBMIT = Object.freeze("SUBMIT");
    static EMPLOYEES_EDIT_BTN_REMOVE = Object.freeze("REMOVE");









    //Menu
        //HeroSection
    static MENU_HERO_TITLE = Object.freeze("Our menu");
    static MENU_HERO_OVERVIEW = Object.freeze("Overview");
    static MENU_HERO_SHOPPINGCART = Object.freeze("See shopping cart");
    static MENU_HERO_ADD_DISH = Object.freeze("Add dish");

    //Menu
        //Menu section
    static MENU_OVERVIEW_TITLE = Object.freeze("Menu overview");
    static MENU_RESTAURANT_SELECTOR_TITLE = Object.freeze("Pick Restaurant");
    static MENU_CARD_PRICE = Object.freeze("Price:");
    static MENU_CARD_DESCRIPTION = Object.freeze("Description:");
    static MENU_CARD_ADD_TO_SHOPPINGCART = Object.freeze("ADD TO SHOPPINGCART");
    static MENU_CARD_REMOVE_FROM_SHOPPINGCART = Object.freeze("REMOVE");
    static MENU_CARD_GO_TO_SHOPPINGCART = Object.freeze("TO CHECKOUT");

    //Menu
        //Edit menu entry section
    static MENU_EDIT_IMG_URL = Object.freeze("Image URL:");
    static MENU_EDIT_NAME = Object.freeze("Name:");
    static MENU_EDIT_PRICE = Object.freeze("Price:");
    static MENU_EDIT_DESCRIPTION = Object.freeze("Description:");
    static MENU_EDIT_BTN_SUBMIT = Object.freeze("SUBMIT");
    static MENU_EDIT_BTN_REMOVE = Object.freeze("REMOVE");


    //Checkout
        //HeroSection
    static CHECKOUT_HERO_TITLE = Object.freeze("Checkout");
    static CHECKOUT_HERO_ALL_ORDERS = Object.freeze("All orders");
    static CHECKOUT_HERO_SHOPPINGCART = Object.freeze("Shopping cart");
    static CHECKOUT_HERO_MENU = Object.freeze("Menu");

    //Checkout
        //shoppingcart section
    static CHECKOUT_SHOPPINGCART_TITLE = Object.freeze("Shopping cart:");
    static CHECKOUT_SHOPPINGCART_CONFIRM_BTN = Object.freeze("CONFIRM");
    static CHECKOUT_SHOPPINGCART_CHANGE_RESTAURANT_BTN = Object.freeze("CHANGE RESTAURANT");
    static CHECKOUT_SHOPPINGCART_EMPTY_CART_TITLE = Object.freeze("The shopping cart is empty..");
    static CHECKOUT_SHOPPINGCART_EMPTY_CART_PARAGRAPH = Object.freeze("Pleas visit the menu page to add items to the cart.");

    static CHECKOUT_CARTLOG_MAIN_TITLE = Object.freeze("Placed orders");
    static CHECKOUT_CARTLOG_TITLE = Object.freeze("Shopping cart:");


    //Statistics
        //Hero section
    static STATISTICS_HERO_TITLE = Object.freeze("Our statistics panel");
    static STATISTICS_HERO_BTN_RESTAURANTS = Object.freeze("Restaurants");
    static STATISTICS_HERO_ECONOMICS = Object.freeze("Economy");
    static STATISTICS_HERO_SALES = Object.freeze("Sales");

    //Statistics
        //Restaurant stats
    static STATISTICS_RESTAURANT_TITLE = Object.freeze("Restaurant statistics")
    static STATISTICS_RESTAURANT_NAME = Object.freeze("Restaurant name:")
    static STATISTICS_RESTAURANT_AVG_PRICE = Object.freeze("Average price sold for:");
    static STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_TEXT = Object.freeze("Most sold items:");
    static STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_NAME = Object.freeze("Name:");
    static STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_AMOUNT = Object.freeze("Sold amount:");

    //Statistics
        //Economics stats
    static STATISTICS_ECONOMICS_TITLE = Object.freeze("Economics statistics");
    static STATISTICS_ECONOMICS_TOTAL_REVENUE = Object.freeze("Total revenue:");
    static STATISTICS_ECONOMICS_TOTAL_SALARIES = Object.freeze("Total salaries:");

    //Statistics
        //Sales stats
    static STATISTICS_SALES_TITLE = Object.freeze("Sales statistics");
    static STATISTICS_SALES_MOST_SOLD_DISH = Object.freeze("Most sold dish:");
    static STATISTICS_SALES_AVG_AMT_PR_CHECKOUT = Object.freeze("Average amount per checkout:");
    static STATISTICS_SALES_TOTAL_REVENUE = Object.freeze("Total revenue:");

}

export default English;