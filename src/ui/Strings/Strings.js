import English from "./lang/English";
import Norweagian from "./lang/Norweagian";

class Strings {
    static selector = Norweagian.LANG_NORWEAGIAN;
    
    static setSelector(selector = Norweagian.LANG_NORWEAGIAN) {
        Strings.selector = selector;
    }
    
    static getHeroWelcome() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.HERO_WELCOME : English.HERO_WELCOME;
    }

    //Nav

    static getNavbarHome() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.NAVBAR_HOME : English.NAVBAR_HOME;
    }

    static getNavbarRestaurants() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.NAVBAR_RESTAURANTS : English.NAVBAR_RESTAURANTS;
    }

    static getNavbarEmployees() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.NAVBAR_EMPLOYEES : English.NAVBAR_EMPLOYEES;
    }

    static getNavbarMenu() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.NAVBAR_MENU : English.NAVBAR_MENU;
    }

    static getNavbarCheckout() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.NAVBAR_CHECKOUT : English.NAVBAR_CHECKOUT;
    }

    static getNavbarStatistics() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.NAVBAR_STATISTICS : English.NAVBAR_STATISTICS;
    }

    //Restaurant

    static getRestaurantsHeroOverview() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_HERO_OVERVIEW : English.RESTAURANTS_HERO_OVERVIEW;
    }

    static RESTAURANTS_OVERVIEW_NO_CONTENT = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_OVERVIEW_NO_CONTENT : English.RESTAURANTS_OVERVIEW_NO_CONTENT;

    static getRestaurantsHeroAddOrder() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_HERO_ADD_ORDER : English.RESTAURANTS_HERO_ADD_ORDER;
    }

    static getRestaurantsHeroAddRestaurants() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_HERO_ADD_RESTAURANT : English.RESTAURANTS_HERO_ADD_RESTAURANT;
    }

    static getRestaurantsHeroTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_HERO_TITLE : English.RESTAURANTS_HERO_TITLE;
    }

    static getRestaurantsOverviewTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_OVERVIEW_TITLE : English.RESTAURANTS_OVERVIEW_TITLE;
    }

    static getRestaurantsCardAdress() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_CARD_ADRESS : English.RESTAURANTS_CARD_ADDRESS;
    }

    static getRestaurantsCardPhone() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_CARD_PHONE : English.RESTAURANTS_CARD_PHONE;
    }

    static getRestaurantsCardCeo() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_CARD_CEO : English.RESTAURANTS_CARD_CEO;
    }

    //Restaurant
        //Edit section
    static RESTAURANTS_EDIT_IMG_URL = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_EDIT_IMG_URL : English.RESTAURANTS_EDIT_IMG_URL;
    static RESTAURANTS_EDIT_NAME = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_EDIT_NAME : English.RESTAURANTS_EDIT_NAME;
    static RESTAURANTS_EDIT_ADDRESS = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_EDIT_ADDRESS : English.RESTAURANTS_EDIT_ADDRESS;
    static RESTAURANTS_EDIT_PHONE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_EDIT_PHONE : English.RESTAURANTS_EDIT_PHONE;
    static RESTAURANTS_EDIT_CEO = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_EDIT_CEO : English.RESTAURANTS_EDIT_CEO;
    static RESTAURANTS_EDIT_BTN_SUBMIT = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_EDIT_BTN_SUBMIT : English.RESTAURANTS_EDIT_BTN_SUBMIT;
    static RESTAURANTS_EDIT_BTN_REMOVE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.RESTAURANTS_EDIT_BTN_REMOVE : English.RESTAURANTS_EDIT_BTN_REMOVE;






    //Employees
    static getEmployeesHeroOverview() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_HERO_OVERVIEW : English.EMPLOYEES_HERO_OVERVIEW;
    }
    
    static getEmployeesHeroAddEmployee() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_HERO_ADD_EMPLOYEE : English.EMPLOYEES_HERO_ADD_EMPLOYEE;
    }

    static getEmployeesSeStatistics() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_HERO_STATISTICS : English.EMPLOYEES_HERO_STATISTICS;
    }

    static getEmployeesHeroTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_HERO_TITLE : English.EMPLOYEES_HERO_TITLE;
    }

    static getEmployeesOverviewTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_OVERVIEW_TITLE : English.EMPLOYEES_OVERVIEW_TITLE;
    }

    static EMPLOYEES_OVERVIEW_NO_EMPLOYEE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_OVERVIEW_NO_EMPLOYEE : English.EMPLOYEES_OVERVIEW_NO_EMPLOYEE;

    static getEmployeesCardAge() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_CARD_AGE : English.EMPLOYEES_CARD_AGE;
    }

    static getEmployeesCardPhone() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_CARD_PHONE : English.EMPLOYEES_CARD_PHONE;
    }

    static getEmployeesCardEmail() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_CARD_EMAIL : English.EMPLOYEES_CARD_EMAIL;
    }

    static getEmployeesCardSalary() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_CARD_SALARY : English.EMPLOYEES_CARD_SALARY;
    }

    //Employees
        //Employees edit section
    static EMPLOYEES_EDIT_IMG_URL = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_EDIT_IMG_URL : English.EMPLOYEES_EDIT_IMG_URL;
    static EMPLOYEES_EDIT_NAME = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_EDIT_NAME : English.EMPLOYEES_EDIT_NAME;
    static EMPLOYEES_EDIT_POSITION = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_EDIT_POSITION : English.EMPLOYEES_EDIT_POSITION;
    static EMPLOYEES_EDIT_AGE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_EDIT_AGE : English.EMPLOYEES_EDIT_AGE;
    static EMPLOYEES_EDIT_SALARY = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_EDIT_SALARY : English.EMPLOYEES_EDIT_SALARY;
    static EMPLOYEES_EDIT_PHONE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_EDIT_PHONE : English.EMPLOYEES_EDIT_PHONE;
    static EMPLOYEES_EDIT_EMAIL = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_EDIT_EMAIL : English.EMPLOYEES_EDIT_EMAIL;
    static EMPLOYEES_EDIT_BTN_SUBMIT = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_EDIT_BTN_SUBMIT : English.EMPLOYEES_EDIT_BTN_SUBMIT;
    static EMPLOYEES_EDIT_BTN_REMOVE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.EMPLOYEES_EDIT_BTN_REMOVE : English.EMPLOYEES_EDIT_BTN_REMOVE;







    //Menu
    static getMenuHeroTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_HERO_TITLE : English.MENU_HERO_TITLE;
    }

    static getMenuHeroOverview() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_HERO_OVERVIEW : English.MENU_HERO_OVERVIEW;
    }
    
    static getMenuHeroAddDish() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_HERO_ADD_DISH : English.MENU_HERO_ADD_DISH;
    }

    static getMenuSeShoppingcart() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_HERO_SHOPPINGCART : English.MENU_HERO_SHOPPINGCART;
    }

    static getMenuOverviewTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_OVERVIEW_TITLE : English.MENU_OVERVIEW_TITLE;
    }

    static getMenuRestaurantSelectorTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_RESTAURANT_SELECTOR_TITLE : English.MENU_RESTAURANT_SELECTOR_TITLE;
    }

    static getMenuCardPrice() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_CARD_PRICE : English.MENU_CARD_PRICE;
    }

    static getMenuCardDescription() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_CARD_DESCRIPTION : English.MENU_CARD_DESCRIPTION;
    }

    static getMenuCardAddToShoppingcart() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_CARD_ADD_TO_SHOPPINGCART : English.MENU_CARD_ADD_TO_SHOPPINGCART;
    }

    static getMenuCardRemoveFromShoppingcart() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_CARD_REMOVE_FROM_SHOPPINGCART : English.MENU_CARD_REMOVE_FROM_SHOPPINGCART;
    }

    static getMenuCardGoToShoppingcart() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_CARD_GO_TO_SHOPPINGCART : English.MENU_CARD_GO_TO_SHOPPINGCART;
    }

    //Menu
        //Edit menu entry section
    static MENU_EDIT_IMG_URL = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_EDIT_IMG_URL : English.MENU_EDIT_IMG_URL;
    static MENU_EDIT_NAME = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_EDIT_NAME : English.MENU_EDIT_NAME;
    static MENU_EDIT_PRICE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_EDIT_PRICE : English.MENU_EDIT_PRICE;
    static MENU_EDIT_DESCRIPTION = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_EDIT_DESCRIPTION : English.MENU_EDIT_DESCRIPTION;
    static MENU_EDIT_BTN_SUBMIT = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_EDIT_BTN_SUBMIT : English.MENU_EDIT_BTN_SUBMIT;
    static MENU_EDIT_BTN_REMOVE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.MENU_EDIT_BTN_REMOVE : English.MENU_EDIT_BTN_REMOVE;







    //Checkout
    static getCheckoutHeroTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.CHECKOUT_HERO_TITLE : English.CHECKOUT_HERO_TITLE;
    }

    static getCheckoutHeroAllOrders() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.CHECKOUT_HERO_ALL_ORDERS : English.CHECKOUT_HERO_ALL_ORDERS;
    }
    
    static getCheckoutHeroShoppingcart() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.CHECKOUT_HERO_SHOPPINGCART : English.CHECKOUT_HERO_SHOPPINGCART;
    }

    static getCheckoutHeroMenu() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.CHECKOUT_HERO_MENU : English.CHECKOUT_HERO_MENU;
    }

    static getCheckoutShoppingcartTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.CHECKOUT_SHOPPINGCART_TITLE : English.CHECKOUT_SHOPPINGCART_TITLE;
    }

    static getCheckoutShoppingcartConfirmBtn() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.CHECKOUT_SHOPPINGCART_CONFIRM_BTN : English.CHECKOUT_SHOPPINGCART_CONFIRM_BTN;
    }

    static getCheckoutShoppingcartChangeRestaurantBtn() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.CHECKOUT_SHOPPINGCART_CHANGE_RESTAURANT_BTN : English.CHECKOUT_SHOPPINGCART_CHANGE_RESTAURANT_BTN;
    }

    static getCheckoutCartLogTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.CHECKOUT_CARTLOG_TITLE : English.CHECKOUT_CARTLOG_TITLE;
    }

    static getCheckoutCartMainTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.CHECKOUT_CARTLOG_MAIN_TITLE : English.CHECKOUT_CARTLOG_MAIN_TITLE;
    }

    static getChekcoutEmptyCartTitle() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.CHECKOUT_SHOPPINGCART_EMPTY_CART_TITLE : English.CHECKOUT_SHOPPINGCART_EMPTY_CART_TITLE;
    }

    static getChekcoutEmptyCartParagraph() {
        return Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.CHECKOUT_SHOPPINGCART_EMPTY_CART_PARAGRAPH : English.CHECKOUT_SHOPPINGCART_EMPTY_CART_PARAGRAPH;
    }

    //Statistics
        //Hero section btns
    static STATISTICS_HERO_TITLE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_HERO_TITLE : English.STATISTICS_HERO_TITLE;
    static STATISTICS_HERO_BTN_RESTAURANTS = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_HERO_BTN_RESTAURANTS : English.STATISTICS_HERO_BTN_RESTAURANTS;
    static STATISTICS_HERO_ECONOMICS = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_HERO_ECONOMICS : English.STATISTICS_HERO_ECONOMICS;
    static STATISTICS_HERO_SALES = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_HERO_SALES : English.STATISTICS_HERO_SALES;

    //Statistics
        //Restaurant stats
    static STATISTICS_RESTAURANT_TITLE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_RESTAURANT_TITLE : English.STATISTICS_RESTAURANT_TITLE;
    static STATISTICS_RESTAURANT_NAME = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_RESTAURANT_NAME : English.STATISTICS_RESTAURANT_NAME;
    static STATISTICS_RESTAURANT_AVG_PRICE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_RESTAURANT_AVG_PRICE : English.STATISTICS_RESTAURANT_AVG_PRICE
    static STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_TEXT = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_TEXT : English.STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_TEXT
    static STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_NAME = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_NAME : English.STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_NAME
    static STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_AMOUNT = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_AMOUNT : English.STATISTICS_RESTAURANT_MOST_SOLD_ITEMS_AMOUNT

    //Statistics
        //Economics stats
    static STATISTICS_ECONOMICS_TITLE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_ECONOMICS_TITLE : English.STATISTICS_ECONOMICS_TITLE;
    static STATISTICS_ECONOMICS_TOTAL_REVENUE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_ECONOMICS_TOTAL_REVENUE : English.STATISTICS_ECONOMICS_TOTAL_REVENUE;
    static STATISTICS_ECONOMICS_TOTAL_SALARIES = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_ECONOMICS_TOTAL_SALARIES : English.STATISTICS_ECONOMICS_TOTAL_SALARIES;

    //Statistics
        //Sales stats
    static STATISTICS_SALES_TITLE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_SALES_TITLE : English.STATISTICS_SALES_TITLE;
    static STATISTICS_SALES_MOST_SOLD_DISH = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_SALES_MOST_SOLD_DISH : English.STATISTICS_SALES_MOST_SOLD_DISH;
    static STATISTICS_SALES_AVG_AMT_PR_CHECKOUT = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_SALES_AVG_AMT_PR_CHECKOUT : English.STATISTICS_SALES_AVG_AMT_PR_CHECKOUT;
    static STATISTICS_SALES_TOTAL_REVENUE = () => Strings.selector === Norweagian.LANG_NORWEAGIAN ? Norweagian.STATISTICS_SALES_TOTAL_REVENUE : English.STATISTICS_SALES_TOTAL_REVENUE;

}

export default Strings;