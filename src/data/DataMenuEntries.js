import MenuEntry from "../domain/model/MenuEntry";
import MenuEntries from "../domain/model/MenuEntries";

export default
class DataMenuEntries {

    static NUM_DATA_ENTRIES_THRESHOLD = Object.freeze(100000);







    static beenInit = false;

    static menuEntries = new MenuEntries();

    static init() {
        return new Promise(
            resolve => {
                const menuEntriesArr = []

                menuEntriesArr.push(
                    new MenuEntry (
                        '1',
                        'CHEESE & TOMATO',
                        '140',
                        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                        'https://www.peppes.no/produktbilder_pepp2013/600x300/P1.jpg'
                    )
                );

                menuEntriesArr.push(
                    new MenuEntry (
                        '2',
                        'H&P HAM',
                        '190',
                        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                        'https://www.peppes.no/produktbilder_pepp2013/600x300/P90.jpg'
                    )
                );

                menuEntriesArr.push(
                    new MenuEntry (
                        '3',
                        'H&P PEPPERONI',
                        '160',
                        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                        'https://www.peppes.no/produktbilder_pepp2013/600x300/P91.jpg'
                    )
                );

                menuEntriesArr.push(
                    new MenuEntry (
                        '4',
                        'H&P HAM & PEPPERONI',
                        '170',
                        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                        'https://www.peppes.no/produktbilder_pepp2013/600x300/P92.jpg'
                    )
                );

                menuEntriesArr.push(
                    new MenuEntry (
                        '5',
                        'HAM & MUSHROOM',
                        '129',
                        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                        'https://www.peppes.no/produktbilder_pepp2013/600x300/P12.jpg'
                    )
                );

                menuEntriesArr.push(
                    new MenuEntry (
                        '6',
                        'THE TROPICAL',
                        '180',
                        'Lorem Ipsum is simply dummy text of the printing and typesetting industry.',
                        'https://www.peppes.no/produktbilder_pepp2013/600x300/P2.jpg'
                    )
                );


                DataMenuEntries.addMenuEntries(menuEntriesArr);

                resolve();
            }
        )
    }

    static initIfNecessary() {
        return new Promise(
            (resolve) => {
                const target = DataMenuEntries.menuEntries.getMenuEntriesArr();
                const isTargetArrEmpty = (tar) => tar.length === 0;

                if (!DataMenuEntries.beenInit && isTargetArrEmpty(target)) {
                    DataMenuEntries.init()
                        .then(() => {
                            DataMenuEntries.beenInit = true;
                            resolve();
                        });
                }
                else
                resolve();
            }
        )
    }

    static getMenuEntriesArr() {
        return new Promise(
            resolve => {
                DataMenuEntries.initIfNecessary()
                    .then(() => resolve(DataMenuEntries.menuEntries.getMenuEntriesArr()))
            }
        )
    }

    static getMenuEntriesMap() {
        return new Promise(
            resolve => {
                DataMenuEntries.initIfNecessary()
                    .then(() => resolve(DataMenuEntries.menuEntries.getMenuEntriesMap()))
            }
        )
    }

    static updateMenuEntry(foodId, dishName, price, dishDescription, imgUrl) {
        return new Promise(
            resolve => {
                DataMenuEntries.getMenuEntriesMap()
                    .then(menuEntriesMap => {
                        DataMenuEntries.addMenuEntry(foodId, dishName, price, dishDescription, imgUrl);
                        resolve (menuEntriesMap.get(foodId))
                    })
            }
        )
    }

    static addMenuEntry(foodId, dishName, price, dishDescription, imgUrl) {
        DataMenuEntries.menuEntries.addMenuEntry(foodId, dishName, price, dishDescription, imgUrl);
    }

    /**
     *
     * @param menuEntriesArr: Array containing several of type menuEntry
     */
    static addMenuEntries(menuEntriesArr = []) {
        menuEntriesArr.forEach(
            (menuEntry) => DataMenuEntries.menuEntries.addMenuEntry(
                menuEntry.foodId,
                menuEntry.dishName,
                menuEntry.price,
                menuEntry.dishDescription,
                menuEntry.imgUrl
            )
        )
    }

    static removeMenuEntry(foodId) {
        DataMenuEntries.menuEntries.removeMenuEntry(foodId);
    }


    static addNewMenuEntry(dishName, price, dishDescription, imgUrl) {
        return new Promise(
            resolve => {
                DataMenuEntries.getMenuEntriesMap()
                    .then(menuEntriesMap => {
                        let valMenuEntryId = Math.floor(Math.random() * DataMenuEntries.NUM_DATA_ENTRIES_THRESHOLD);
                        while (Array.from(menuEntriesMap.keys()).includes("" + valMenuEntryId))
                            valMenuEntryId = Math.floor(Math.random() * DataMenuEntries.NUM_DATA_ENTRIES_THRESHOLD);
        
                        const foodId = "" + valMenuEntryId;
        
        
                        DataMenuEntries.addMenuEntry(foodId, dishName, price, dishDescription, imgUrl)
        
                        resolve (menuEntriesMap.get(foodId))
                    })
            }
        )
    }

}
