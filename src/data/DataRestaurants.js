import Restaurants from "../domain/model/Restaurants";
import Restaurant from "../domain/model/Restaurant";


/**
 *      Static Data Gateway implemented only using members of the static scope to enforce
 *      a singleton without needing the "getInstance" method.
 */

export default
class DataRestaurants {

    static NUM_DATA_ENTRIES_THRESHOLD = Object.freeze(100000);







    static beenInit = false;

    static restaurants =  new Restaurants();

    static init(){
        return new Promise(
            (resolve) => {
                const restaurantArr = []

                restaurantArr.push(
                    new Restaurant(
                        "1",
                        "Majorstuen",
                        "https://tellusdmsmedia.newmindmedia.com/wsimgs/Olivia-Ostbanehallen_2_1729450996.jpg[WallImage][491E7F0DBADE2AD4438FEA6F4A19E84D]",
                        "51515251",
                        "Majorstueveien 35, 0357 Oslo",
                        "1"
                    )
                );
                restaurantArr.push(
                    new Restaurant(
                        "2",
                        "Grûnerløkka",
                        "https://assets.simpleviewcms.com/simpleview/image/fetch/c_limit,h_1200,q_75,w_1200/https://media.newmindmedia.com/TellUs/image/%3Ffile%3DFoyn_1_1150299673.jpg%26dh%3D533%26dw%3D800%26t%3D4",
                        "51515252",
                        "Grunerløkkaveien 37, 0787 Oslo",
                        "2"
                    )
                );
                restaurantArr.push(
                    new Restaurant(
                        "3",
                        "Karl Johan",
                        "https://www.norskgjenvinning.no/ims/26780692143/Restaurant-3.jpg?crop=430,728,4262,2557&width=1280&height=768&format=jpg&quality=60",
                        "51515253",
                        "Karl Johans gate 107, 0787 Oslo",
                        "3"
                    )
                );
                restaurantArr.push(
                    new Restaurant(
                        "4",
                        "Tøyen",
                        "https://images.dn.no/image/dzlhZXZ0bFRJWmw2RGJ3YUEydTBibUZiREZLK0JSaDZkeEw2L2l5WU9URT0=/nhst/binary/3e1635f96f25a3c623bdbdd03dada1dd",
                        "51515254",
                        "Tøyens gate 17, 0787 Oslo",
                        "4"
                    )
                );
                DataRestaurants.addRestaurants(restaurantArr);


                resolve();
            }
        )


    }

    static initIfNecessary() {
        return new Promise(
            (resolve) => {
                const target = DataRestaurants.restaurants.getRestaurantsArr();
                const isTargetArrEmpty = (tar) => tar.length === 0;

                if (!DataRestaurants.beenInit && isTargetArrEmpty(target)) {
                    DataRestaurants.init()
                        .then(() => {
                            DataRestaurants.beenInit = true;
                            resolve();
                        });
                }
                else
                resolve();
            }
        )
    }

    static getRestaurants(){
        return new Promise(
            resolve => {
                DataRestaurants.initIfNecessary()
                    .then(() => resolve(DataRestaurants.restaurants.getRestaurantsArr()))
            }
        )
    }

    static addRestaurant(restaurantId, name, imgUrl, tlfnr, address, manager){
        DataRestaurants.restaurants.setRestaurant(restaurantId, name, imgUrl, tlfnr, address, manager);
    }

    static addNewRestaurant(name, imgUrl, tlfnr, address, managerEmployeeNum) {

        let valRestaurantId = Math.floor(Math.random() * DataRestaurants.NUM_DATA_ENTRIES_THRESHOLD);
        while (Array.from(DataRestaurants.restaurants.getRestaurantsMap().keys()).includes("" + valRestaurantId))
            valRestaurantId = Math.floor(Math.random() * DataRestaurants.NUM_DATA_ENTRIES_THRESHOLD);

        const restaurantId = "" + valRestaurantId;


        DataRestaurants.addRestaurant(restaurantId, name, imgUrl, tlfnr, address, managerEmployeeNum)

        return DataRestaurants.getRestaurant(restaurantId);
    }

    /**
     * 
     * @param restaurantArr: Array containing several restaurants of type Restaurant
     */
    static addRestaurants(restaurantArr = []){
        restaurantArr.forEach(
            (restaurant) => DataRestaurants.restaurants.setRestaurant(
                restaurant.restaurantId,
                restaurant.name,
                restaurant.imgUrl,
                restaurant.tlfnr,
                restaurant.address,
                restaurant.managerEmployeeNum
            )
        )
    }

    static removeRestaurant(restaurantId){
        console.log(restaurantId)
        DataRestaurants.restaurants.removeRestaurant(restaurantId);
    }

    static getRestaurant(id) {
        return new Promise(
            resolve => {
                DataRestaurants.initIfNecessary()
                    .then(() => {
                        resolve(DataRestaurants.restaurants.getRestaurantById(id))
                    })
            }
        )
    }

    static updateRestaurant(restaurantId, name, imgUrl, tlfnr, address, managerEmployeeNum) {
        this.restaurants.setRestaurant(restaurantId, name, imgUrl, tlfnr, address, managerEmployeeNum);
        return this.restaurants.getRestaurantById(restaurantId)
    }
}