import Checkout from "../domain/model/Checkout";
import Checkouts from "../domain/model/Checkouts";
import DataMenuEntries from "./DataMenuEntries";

export default
class DataCheckouts {
    static checkouts = new Checkouts();

    static init() {
        return new Promise(
            resolve => {
                DataMenuEntries.getMenuEntriesArr()
                    .then(
                        (menuEntries) => {
                            const menuEntriesArr = menuEntries;
                            const menuEntriesArr2 = [menuEntriesArr[0], menuEntriesArr[1]];
                            const menuEntriesArr3 = [menuEntriesArr[2], menuEntriesArr[3], menuEntriesArr[4]];
                            const menuEntriesArr4 = [menuEntriesArr[3], menuEntriesArr[4], menuEntriesArr[5], menuEntriesArr[2]];
                            const menuEntriesArr5 = [menuEntriesArr[0], menuEntriesArr[1], menuEntriesArr[2], menuEntriesArr[3], menuEntriesArr[4]];

                            DataCheckouts.checkouts
                                .addCheckout(
                                    "22",
                                    new Checkout(
                                        "1",
                                        menuEntriesArr2
                                    )
                                )
                            DataCheckouts.checkouts
                                .addCheckout(
                                    "23",
                                    new Checkout(
                                        "2", menuEntriesArr4
                                    )
                                )
                            DataCheckouts.checkouts
                                .addCheckout(
                                    "453",
                                    new Checkout(
                                        "4", menuEntriesArr5
                                    )
                                )
                            DataCheckouts.checkouts
                                .addCheckout(
                                    "263",
                                    new Checkout(
                                        "3", menuEntriesArr3
                                    )
                                )

                            resolve();
                        }
                    );
            }
        )
    }

    static initIfNecessary() {
        return new Promise(
            (resolve) => {
                const checkoutArr = DataCheckouts.checkouts.getCheckoutsArr();
                const isCheckoutsArrEmpty = (checkoutArr) => checkoutArr.length <= 0;

                if (isCheckoutsArrEmpty(checkoutArr)) {
                    DataCheckouts.init()
                        .then(() => resolve());
                }
                else
                resolve();
            }
        )
    }

    static getCheckoutsMap(){
        return new Promise(
            resolve => {
                DataCheckouts.initIfNecessary()
                    .then(() => resolve(DataCheckouts.checkouts.getCheckoutsMap()))
            }
        )
    }

    static getCheckoutIdToCheckoutMap(){
        return new Promise(
            resolve => {
                DataCheckouts.initIfNecessary()
                    .then(() => resolve(DataCheckouts.checkouts.getCheckoutIdToCheckoutMap()))
            }
        )
    }

    static getCheckoutsArr(){
        return new Promise(
            resolve => {
                DataCheckouts.initIfNecessary()
                    .then(() => resolve(DataCheckouts.checkouts.getCheckoutsArr()))
            }
        )
    }

    /**
     *
     * @param restaurantId : String
     * @param menuEntries : Array of type MenuEntry
     */
    static addCheckout(restaurantId, menuEntries) {
        let newId = "" + (Math.random() * 100000);
        while (DataCheckouts.checkouts.getCheckoutsMap().get(newId))
            newId = "" + (Math.random() * 100000);


        DataCheckouts.checkouts.addCheckout(
            newId,
            new Checkout(restaurantId, menuEntries, newId)
        )
    }

}