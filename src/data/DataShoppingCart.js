import Checkout from "../domain/model/Checkout";
import DataCheckouts from "./DataCheckouts";


export default class DataShoppingCart {

    static beenInit = false;

    static order = new Checkout("", []);

    static init() {
        return new Promise(
            resolve =>
                DataCheckouts.getCheckoutsArr()
                    .then(
                        (checkouts) => {
                            //commented away to prevent init of shopping cart for production deployment.
                            //Uncomment the following two lines if you wish to use an pre initialized shopping cart.

                            //DataShoppingCart.order = checkouts[1];
                            //DataShoppingCart.order.restaurantId = "";
                            resolve();
                        }
                    )
        )
    }

    static initIfNecessary() {
        return new Promise(
            resolve => {
                const target = DataShoppingCart.order.menuEntries;
                const isTargetArrEmpty = (tar) => tar.length === 0;

                if (!DataShoppingCart.beenInit && isTargetArrEmpty(target)) {
                    DataShoppingCart.init()
                        .then(() => {
                            DataShoppingCart.beenInit = true;
                            resolve();
                        });
                }
                else
                resolve();
            }
        )
    }

    static addMenuEntry(restaurantId = "", menuEntry){
        if (restaurantId !== ""){
            if (restaurantId !== DataShoppingCart.order.restaurantId)
                DataShoppingCart.order = new Checkout(restaurantId, []);
        }
        DataShoppingCart.order.addMenuEntry(menuEntry);
    }

    static clearCart() {
        DataShoppingCart.order = new Checkout("", []);
    }

    static removeMenuEntry(menuEntryId){
        DataShoppingCart.order.removeMenuEntry(menuEntryId);
    }

    static getShoppingCart(){
        return new Promise(
         resolve => {
             DataShoppingCart.initIfNecessary()
                 .then(
                     () =>{
                         resolve({
                             restaurantID: DataShoppingCart.order.getRestaurantId(),
                             menuEntries: DataShoppingCart.order.getMenuEntriesArr()
                         })
                     }
                 )
         }
        )
    }

    static getRestaurantId(){
        return DataShoppingCart.order.getRestaurantId();
    }

    static getMenuEntries(){
        return new Promise(
            resolve => {
                DataShoppingCart.initIfNecessary()
                    .then(
                        () =>{
                            resolve(
                                DataShoppingCart.order.getMenuEntriesArr()
                            )
                        }
                    )
            }
           )
    }

    static setRestaurantId(restaurantId) {
        DataShoppingCart.order = new Checkout(restaurantId, []);
    }

}