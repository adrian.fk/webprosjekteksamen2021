import DataShoppingCart from "./DataShoppingCart";
import DataCheckouts from "./DataCheckouts";
import DataMenuEntries from "./DataMenuEntries";
import DataRestaurants from "./DataRestaurants";
import DataEmployees from "./DataEmployees";


export default class DataStatistics{

    static commitShoppingCart(){
        return new Promise(
            (resolve) => {
                DataShoppingCart.getMenuEntries()
                    .then(
                        menuEntries => {
                            DataCheckouts.addCheckout(DataShoppingCart.getRestaurantId(), menuEntries)
                            DataShoppingCart.clearCart();
                            resolve();
                            
                        }
                    )
            }
        )
    }

    static getAllOrdersArr(restaurantId){
        return new Promise(
            resolve => {
                DataCheckouts.getCheckoutsMap()
                    .then(
                        map => {
                            const allOrdersArr = DataStatistics._accumulateMenuEntriesArrUponCheckoutsArr(map.get(restaurantId))
                            resolve(allOrdersArr)
                        }
                    )
            }
        )
    }

    static getTotalRevenueByRestaurant(restaurantId){
        return new Promise(
            resolve => {
                DataStatistics.getAllOrdersArr(restaurantId)
                    .then(allOrdersArr => {
                        resolve(DataStatistics._generateTotalRevenue(allOrdersArr))
                    })
            }
        )
    }

    static _generateTotalRevenue(ordersArr) {
        let totalRevenue = 0;

        if (ordersArr)
        for (const order of ordersArr) {
            totalRevenue += Number(order.price);
        }
        return totalRevenue;
    }

    static _generateTotalRevenueUpnCheckoutsArr(checkoutsArr) {
        let totalRevenue = 0;

        if (checkoutsArr && checkoutsArr.menuEntries?.length) {
            return DataStatistics._generateTotalRevenue(checkoutsArr.menuEntries);
        }
        if (checkoutsArr)
        for (const checkout of checkoutsArr) {
            totalRevenue += DataStatistics._generateTotalRevenue(checkout?.menuEntries);
        }
        return totalRevenue;
    }

    static getTotalRevenue(){
        return new Promise(
            resolve => {
                DataCheckouts.getCheckoutsArr()
                    .then((checkoutsArr) => {
                        resolve(DataStatistics._generateTotalRevenueUpnCheckoutsArr(checkoutsArr))
                    })
            }
        )
    }

    static getAvgPriceByRestaurant(restaurantId){
        return new Promise(
            resolve => DataStatistics.getAllOrdersArr(restaurantId)
                .then(ordersArr => {
                    console.log(ordersArr)
                    let avgPrice = "0";
                    let totalRevenue = DataStatistics._generateTotalRevenue(ordersArr);
                    if (ordersArr.length) {
                        avgPrice = totalRevenue / ordersArr.length;
                    }
                    console.log(avgPrice)


                    resolve(avgPrice)
                })
        )
    }

    static getAvgPrice(){
        return new Promise(
            resolve => {
                DataCheckouts.getCheckoutsArr()
                    .then((checkoutsArr) => {
                        let totalRevenue = DataStatistics._generateTotalRevenueUpnCheckoutsArr(checkoutsArr);

                        const avgPrice = totalRevenue / checkoutsArr.length;

                        resolve(avgPrice);
                    })
            }
        )
    }

    static getNumberOfRestaurants(){
        return new Promise(
            resolve => {
                DataRestaurants.getRestaurants()
                    .then(
                        restaurants => resolve(restaurants.length)
                    );
            }
        )
    }

    static getNumberOfMenuEntries(){
        return new Promise(
            (resolve) => DataMenuEntries.getMenuEntriesArr()
                                    .then(menuEntriesArr => resolve(menuEntriesArr.length))
        )
    }

    static getMostSoldArr(){
        return new Promise(
            (resolve) => {
                DataCheckouts.getCheckoutsArr()
                    .then(
                        checkoutsArr => {

                            const out = DataStatistics._generateSortedArray(checkoutsArr);

                            resolve(out);
                        }
                    )
            }
        )
    }

    static _generateSortedArray(inputOrdersArr) {
        const numOrdersByFoodId = new Map();

        let menuEntries = inputOrdersArr;
        if (inputOrdersArr?.menuEntries?.length > 0) {
            menuEntries = inputOrdersArr.menuEntries;
        }
        if (inputOrdersArr[0]?.checkoutId?.length) {

            for (const checkout of inputOrdersArr) {
                this._setUpHelpersUponMenuEntries(checkout.menuEntries, numOrdersByFoodId);
            }
        }
        else
        this._setUpHelpersUponMenuEntries(menuEntries, numOrdersByFoodId);

        const numOrdersArray = Array.from(numOrdersByFoodId.values());
        const numOrdersIdArray = Array.from(numOrdersByFoodId.keys());

        DataStatistics._sortOrdersArray(numOrdersArray, numOrdersIdArray);

        const out = [];
        for (let i = 0; i < numOrdersArray.length; i++) {
            out.push({
                foodId: numOrdersIdArray[i],
                soldUnits: numOrdersArray[i]
            });
        }
        return out;
    }

    static _setUpHelpersUponMenuEntries(menuEntries, numOrdersByFoodId) {
        for (const menuEntry of menuEntries) {
            if (numOrdersByFoodId && menuEntry.foodId) {
                let target = numOrdersByFoodId.get(menuEntry.foodId);
                if (!target) target = 0;
                ++target;
                numOrdersByFoodId.set(menuEntry.foodId, target);

            }
        }
    }

    static _sortOrdersArray(numOrdersArray, numOrdersIdArray) {
        for (let i = 0; i < numOrdersArray.length; i++) {
            for (let j = 0; j < numOrdersArray.length - i - 1; j++) {
                if (numOrdersArray[j] < numOrdersArray[j + 1]) {
                    let temp = numOrdersArray[j];
                    let tempKey = numOrdersIdArray[j];
                    numOrdersArray[j] = numOrdersArray[j + 1];
                    numOrdersArray[j + 1] = temp;
                    numOrdersIdArray[j] = numOrdersIdArray[j + 1];
                    numOrdersIdArray[j + 1] = tempKey;
                }
            }
        }
    }

    static _accumulateMenuEntriesArrUponCheckoutsArr(checkoutsArr) {
        let menuEntries = [];

        if(checkoutsArr)
        for (const checkout of checkoutsArr) {
            menuEntries = [...menuEntries, ...checkout.menuEntries];
        }

        return menuEntries;
    }

    static getMostSoldArrByRestaurant(restaurantId){
        return new Promise(
            resolve => {
                DataCheckouts.getCheckoutsMap()
                    .then(
                        (checkoutsMap) => {
                            if (checkoutsMap.get(restaurantId)) {
                                const checkoutsArr = checkoutsMap.get(restaurantId);
                                const ordersArr = DataStatistics._accumulateMenuEntriesArrUponCheckoutsArr(checkoutsArr);

                                console.log(ordersArr)

                                const out = DataStatistics._generateSortedArray(ordersArr);

                                console.log(out)
                                resolve(out);
                            }
                            else resolve([]);
                        }
                    )
            }
        )
    }

    static getMostSoldMenuEntry(){
        return new Promise(
            resolve => {
                DataStatistics.getMostSoldArr()
                    .then(mostSoldOrders => resolve(mostSoldOrders.length > 0 ? mostSoldOrders[0] : null))
            }
        )

    }
    
    static getTotalSalary(){
        return new Promise(
            resolve => {
                DataEmployees.getEmployees()
                    .then(employees => {

                        let totalSalary = 0;

                        for (const employee of employees) {
                            if (Number(employee.salary))
                                totalSalary += Number(employee.salary);
                        }

                        resolve(totalSalary);
                    })
            }
        )
    }

}