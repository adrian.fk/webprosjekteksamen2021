import Employees from "../domain/model/Employees";
import Employee from "../domain/model/Employee";


export default
class DataEmployees {
    static beenInit = false;

    static employees = new Employees();

    static init() {
        return new Promise(
            resolve => {
                const employeeArr = []

                employeeArr.push(
                    new Employee(
                        "1",
                        "Ola Nordmann",
                        33,
                        "ola.nordmann@lol.com",
                        "22225555",
                        "https://sykepleien.no/sites/default/files/styles/facebook/public/2020-03/vasker_hendene_doblet_og_beskaret.jpg",
                        20000,
                        "CEO"
                    )
                );
                employeeArr.push(
                    new Employee(
                        "2",
                        "Kari Larsen",
                        35,
                        "kari.larsen@lol.com",
                        "55552222",
                        "https://upload.wikimedia.org/wikipedia/commons/thumb/6/63/Kari_Traa.jpg/1200px-Kari_Traa.jpg",
                        10000,
                        "Waiter"
                    )
                );
                employeeArr.push(
                    new Employee("3",
                        "Lars Olavsson",
                        23,
                        "lars.olavsson@lol.com",
                        "44332556",
                        "https://upload.wikimedia.org/wikipedia/commons/0/06/LarsUlrich.jpg",
                        18000,
                        "Waiter"
                    )
                );
                employeeArr.push(
                    new Employee(
                        "4",
                        "Erik Monsen",
                        33,
                        "erik@lol.com",
                        "22225555",
                        "https://upload.wikimedia.org/wikipedia/commons/5/52/Erik_Thorstvedt_%286188654618%29_%28cropped%29.jpg",
                        10000,
                        "Renhold"
                    )
                );

                employeeArr.push(
                    new Employee(
                        "5",
                        "Frida Jensen",
                        66,
                        "frida.jensen@lol.com",
                        "44665577",
                        "https://upload.wikimedia.org/wikipedia/commons/0/09/Frida_Kahlo%2C_by_Guillermo_Kahlo_%28cropped%29.jpg",
                        40000,
                        "Renhold"
                    )
                );

                DataEmployees.addEmployees(employeeArr);


                resolve();
            }
        )
    }

    static initIfNecessary() {
        return new Promise(
            (resolve) => {
                const target = DataEmployees.employees.getEmployeesArr();
                const isTargetArrEmpty = (tar) => tar.length === 0;

                if (!DataEmployees.beenInit && isTargetArrEmpty(target)) {
                    DataEmployees.init()
                        .then(() => {
                            DataEmployees.beenInit = true;
                            resolve();
                        });
                }
                else
                resolve();
            }
        )
    }

    static getEmployees() {
        return new Promise(
            resolve => {
                DataEmployees.initIfNecessary()
                    .then(() => resolve(DataEmployees.employees.getEmployeesArr()))
            }
        )
    }

    static getEmployeesMap() {
        return new Promise(
            resolve => {
                DataEmployees.initIfNecessary()
                    .then(() => resolve(DataEmployees.employees.getEmployeesMap()))
            }
        )
    }

    static addEmployee(employeeNumber, name, age, email, tlfnr, imgUrl, salary, title) {
        DataEmployees.employees.addEmployee(employeeNumber, name, age, email, tlfnr, imgUrl, salary, title);
    }

    static addNewEmployee(name, age, email, tlfnr, imgUrl, salary, title) {

        let valEmployeeNumber = 1337;
        while (Array.from(DataEmployees.employees.getEmployeesMap().keys()).includes("" + valEmployeeNumber))
            valEmployeeNumber = Math.random() * DataEmployees.NUM_DATA_ENTRIES_TRESHOLD;

        const employeeNumber = "" + valEmployeeNumber;


        DataEmployees.addEmployee(employeeNumber, name, age, email, tlfnr, imgUrl, salary, title);

        return DataEmployees.getEmployee(employeeNumber);
    }

    /**
     *
     * @param employeesArr: Array containing several EmployeesView of type Employee
     */
    static addEmployees(employeesArr = []) {
        employeesArr.forEach(
            (employee) => DataEmployees.employees.addEmployee(
                employee.employeeNumber,
                employee.name,
                employee.age,
                employee.email,
                employee.tlfnr,
                employee.imgUrl,
                employee.salary,
                employee.title
            )
        )
    }

    static removeEmployee(employeeNumber) {
        DataEmployees.employees.removeEmployee(employeeNumber);
    }

    static getEmployee(employeeNumber) {
        return new Promise(
            resolve => {
                DataEmployees.initIfNecessary()
                    .then(() => resolve(DataEmployees.employees.getEmployee(employeeNumber)))
            }
        )
    }

    static updateEmployee(employeeNumber, name, age, email, tlfnr, imgurl, salary, title) {
        this.employees.addEmployee(employeeNumber, name, age, email, tlfnr, imgurl, salary, title);
        return this.employees.getEmployee(employeeNumber)
    }

}
