import Person from "./Person";


export default
class Employee extends Person {

    /**
     *
     * @param employeeNumber: String
     * @param name: String
     * @param age: Number
     * @param email: String
     * @param tlfnr: String
     * @param imgUrl: String
     * @param salary: Number
     * @param title: String
     */
    constructor(employeeNumber, name, age, email, tlfnr, imgUrl, salary, title) {
        super(name, age, email, tlfnr, imgUrl)
        this.employeeNumber = employeeNumber;
        this.salary = salary;

        //Title represents the work title of the employee
        this.title = title;
    }
}