export default
class Restaurant {
/**
 * 
 * @param restaurantId: String
 * @param imgUrl: String
 * @param name: String
 * @param tlfnr: String
 * @param address: String
 * @param managerEmployeeNum: String
 */

    constructor(restaurantId, name, imgUrl, tlfnr, address, managerEmployeeNum) {
        this.restaurantId = restaurantId;
        this.name = name;
        this.imgUrl = imgUrl;
        this.tlfnr = tlfnr;
        this.address = address;
        this.managerEmployeeNum = managerEmployeeNum;
    }
}