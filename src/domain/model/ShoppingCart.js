import Checkout from "./Checkout";


export default class ShoppingCart {

    static order = new Checkout("", []);

    static addMenuEntry(restaurantId, menuEntry){
        if (restaurantId !== ""){
            if (restaurantId !== ShoppingCart.order.restaurantId)
                ShoppingCart.order = new Checkout(restaurantId, []);
        }
        ShoppingCart.order.addMenuEntry(menuEntry);
    }

    static removeMenuEntry(menuEntryId){
        ShoppingCart.order.Checkout.removeMenuEntry(menuEntryId);
    }

    static getShoppingCart(){
        return {restaurantID: ShoppingCart.order.restaurantId, menuEntries: ShoppingCart.order.menuEntriesArr}
    }

    static getRestaurantId(){
        return ShoppingCart.order.restaurantId;
    }

    static getMenuEntries(){
        return ShoppingCart.order.getMenuEntriesArr();
    }

}