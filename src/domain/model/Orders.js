import Order from "../../domain/model/Order";


export default
class Orders {

    constructor() {
        this.orders = new Map();
    }

    addOrder(orderID, restaurantID) {
        this.orders.set(orderID, new Order(orderID, restaurantID));
    }

    removeOrder(orderID) {
        this.orders.delete(orderID);
    }
}