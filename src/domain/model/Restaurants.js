
import Restaurant from "./Restaurant";


export default
class Restaurants {

    constructor() {
        this.resturants = new Map();
    }

    setRestaurant(restaurantId, name, imgUrl, tlfnr, adress, managerEployeeNum) {
        this.resturants.set(restaurantId, new Restaurant(restaurantId, name, imgUrl, tlfnr, adress, managerEployeeNum));
    }

    removeRestaurant(restaurantId) {
        this.resturants.delete(restaurantId);
    }

    getRestaurantsMap() {
        return this.resturants;
    }

    getRestaurantById(id) {
        return this.resturants.get(id);
    }

    getRestaurantsArr() {
        return Array.from(this.resturants.values());
    }
}