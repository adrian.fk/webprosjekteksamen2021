import MenuEntry from "./MenuEntry";

export default
class Checkout {
    /**
     *
     * @param {*} restaurantId: String
     * @param {*} menuEntries: Array containing type menuEntry
     * @param checkoutId : checkout identifier
     */
    constructor( restaurantId, menuEntries, checkoutId ) {
        this.restaurantId = restaurantId;
        this.checkoutId = checkoutId;
        this.menuEntriesMap = new Map();
        this.menuEntries = []
        for (const element of menuEntries) {
            if (element) {
                const menuEntry = new MenuEntry(element.foodId, element.dishName, element.price, element.dishDescription, element.imgUrl)
                this.menuEntries.push(menuEntry)
                this.menuEntriesMap.set(element.foodId, element);
            }
        }
    }

    getRestaurantId() {
        return this.restaurantId;
    }

    addMenuEntry(menuEntry){
        this.menuEntries.push(menuEntry);
        this.menuEntriesMap.set(menuEntry.foodId, menuEntry);
    }

    removeMenuEntry(foodId){
        this.menuEntriesMap.delete(foodId);
        this.menuEntries = this.getMenuEntriesArr();
    }

    getMenuEntriesArr(){
        return Array.from(this.menuEntriesMap.values());
    }

    getCheckoutId() {
        return this.checkoutId;
    }


}
