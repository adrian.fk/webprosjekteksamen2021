

export default

class MenuEntry {

    /**
     * 
     * @param foodId: String  
     * @param dishName: String  
     * @param price: Number 
     * @param dishDescription: String  
     * @param imgUrl: String 
     */
    constructor(foodId, dishName, price, dishDescription, imgUrl){
        this.foodId = foodId;
        this.dishName = dishName;
        this.price = price;
        this.dishDescription = dishDescription;
        this.imgUrl = imgUrl;
    }
}