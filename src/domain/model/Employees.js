import Employee from "./Employee";


export default
class Employees {

    constructor() {
        this.employees = new Map();
    }

    addEmployee(employeeNumber, name, age, email, tlfnr, imgUrl, salary, title) {
        this.employees.set(employeeNumber, new Employee(employeeNumber, name, age, email, tlfnr, imgUrl, salary, title));
    }

    removeEmployee(employeeNumber) {
        this.employees.delete(employeeNumber);
    }

    getEmployeesMap() {
        return this.employees;
    }

    getEmployeesArr() {
        return Array.from(this.employees.values());
    }

    getEmployee(employeeNumber) {
        return this.employees.get(employeeNumber);
    }
}