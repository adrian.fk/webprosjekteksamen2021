import Checkout from "./Checkout";

export default
class Checkouts {
    constructor() {
        this.checkoutsMap = new Map();
        this.checkoutsIdToCheckoutMap = new Map();
    }

    addCheckout(checkoutId, checkout){
        const out = new Checkout(checkout.restaurantId, checkout.menuEntries, checkoutId);
        const checkoutsArr = this.checkoutsMap.get(checkout.restaurantId);
        if (checkoutsArr) {
            const newCheckoutsArr = [...checkoutsArr, out];
            this.checkoutsMap.set(checkout.restaurantId, newCheckoutsArr)
        }
        else {
            this.checkoutsMap.set(checkout.restaurantId, [out]);
        }
        this.checkoutsIdToCheckoutMap.set(checkoutId, out);
    }

    getCheckoutsMap(){
        return this.checkoutsMap;
    }

    getCheckoutIdToCheckoutMap() {
        return this.checkoutsIdToCheckoutMap;
    }

    getCheckoutsArr(){
        return Array.from(this.checkoutsIdToCheckoutMap.values());
    }
    
    
}