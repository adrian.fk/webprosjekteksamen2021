

export default
class Person {

    constructor(name, age, email, tlfnr, imgUrl) {
        this.name = name;
        this.age = age;
        this.email = email;
        this.tlfnr = tlfnr;
        this.imgUrl = imgUrl;
    }
}