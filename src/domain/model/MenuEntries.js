import MenuEntry from "./MenuEntry";

export default
class MenuEntries {
 
    constructor() {
        this.menuEntries = new Map();
    }

    addMenuEntry(foodId, dishName, price, dishDescription, imgUrl) {
        this.menuEntries.set(foodId, new MenuEntry(foodId, dishName, price, dishDescription, imgUrl))
    }

    removeMenuEntry(foodId) {
        this.menuEntries.delete(foodId);
    } 

    getMenuEntriesMap(){
        return this.menuEntries;
    }

    getMenuEntriesArr() {
        return Array.from(this.menuEntries.values());
    }
}