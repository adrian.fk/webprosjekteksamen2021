

const StatisticsPresenter = (() => {

    let _statisticsView;
    let _salesViewWidget;
    let _restaurantsViewWidget;
    let _economicsViewWidget;

    const bindStatisticsView = (viewInstance) => {
        _statisticsView = viewInstance;
    }

    const bindSalesViewWidget = (viewInstance) => {
        _salesViewWidget = viewInstance;
    }

    const bindRestaurantsViewWidget = (viewInstance) => {
        _restaurantsViewWidget = viewInstance;
    }

    const bindEconomicsViewWidget = (viewInstance) => {
        _economicsViewWidget = viewInstance;
    }

    const _parseMostSoldItems = (restaurantId, mostSoldItemsPerRestaurantArr) => {
        for (const mostSoldItem of mostSoldItemsPerRestaurantArr) {
            if (mostSoldItem.restaurantId === restaurantId) return mostSoldItem.mostSoldItems;
        }
        return [];
    }

    const _parseAvgPriceOfSoldItems = (restaurantId, avgPriceSoldByRestaurant) => {
        for (const avgPriceSold of avgPriceSoldByRestaurant) {
            if (restaurantId === avgPriceSold.restaurantId) return avgPriceSold.avgPriceSoldRestaurant;
        }
        return "";
    }

    const presentRestaurantStatistics = (response = {restaurants: [], mostSoldItemsPerRestaurant: [], avgPriceSoldByRestaurant: []}) => {
        console.log(response)
        const viewModel = [];
        for (const restaurant of response.restaurants) {
            const mostSoldItems = _parseMostSoldItems(restaurant.restaurantId, response.mostSoldItemsPerRestaurant);
            const avgPriceSoldItem = _parseAvgPriceOfSoldItems(restaurant.restaurantId, response.avgPriceSoldByRestaurant);

            viewModel.push({...restaurant, mostSoldItems, avgPriceSoldItem})
        }

        _restaurantsViewWidget.displayRestaurants(viewModel);
    }

    const presentEconomicsStatistics = (response = {totalRevenue: "", totalSalary: ""}) => {
        _economicsViewWidget.displayEconomics(response.totalRevenue, response.totalSalary);
    }

    const presentSalesStats = (response = {mostSoldEntry: {}, totalAvgSold: 0,  resRestaurants: []}) => {
        _salesViewWidget.displaySalesWidget(response.resRestaurants, response.mostSoldEntry, response.totalAvgSold)
    }





    return {
        bindStatisticsView,
        bindSalesViewWidget,
        bindRestaurantsViewWidget,
        bindEconomicsViewWidget,
        presentRestaurantStatistics,
        presentEconomicsStatistics,
        presentSalesStats,
    }

}) ();

export default StatisticsPresenter;