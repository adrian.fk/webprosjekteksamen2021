

const RestaurantsPresenter = (() => {

    let _restaurantsView;
    let _editRestaurantView;

    const bindView = (view) => {
        _restaurantsView = view;
    }

    const bindViewEditRestaurantView = (view) => {
        _editRestaurantView = view;
    }

    const findManagerOfRestaurant = (restaurantManagerId = "", employeesArr = []) => {
        for (const employee of employeesArr){
            if (employee.employeeNumber === restaurantManagerId) {
                return employee;
            }
        }

        return undefined;
    }

    const presentRestaurants = (response = {restaurants: [], employees: []}) => {
        const viewModel = [];
        response.restaurants.forEach(
            (restaurant) => {
                const restaurantManager = findManagerOfRestaurant(restaurant.managerEmployeeNum, response.employees)
                viewModel.push({
                    id: restaurant.restaurantId,
                    name: restaurant.name,
                    imgUrl: restaurant.imgUrl,
                    tlfnr: restaurant.tlfnr,
                    address: restaurant.address,
                    managerName: restaurantManager?.name || ""
                })
            }
        )
        _restaurantsView.displayRestaurants(viewModel);
    }

    const hideEditRestaurantView = () => {
        _restaurantsView?.hideEditRestaurantView();
    }

    const presentEditRestaurant = (id, restaurant) => {
        if (restaurant) {
            //Update view
            _editRestaurantView?.updateViewState(
                restaurant.id,
                restaurant.name,
                restaurant.imgUrl,
                restaurant.tlfnr,
                restaurant.address,
                restaurant.managerEmployeeNum
            )
        }
        else {
            //Open view
            _restaurantsView?.displayEditRestaurantView(id);
        }
    }

    const presentUpdateRestaurants = (id) => {
        hideEditRestaurantView();
        _restaurantsView?.rerender();
    }


    return {
        bindView,
        bindViewEditRestaurantView,
        presentRestaurants,
        presentEditRestaurant,
        hideEditRestaurantView,
        presentUpdateRestaurants
    }
}) ();

export default RestaurantsPresenter;