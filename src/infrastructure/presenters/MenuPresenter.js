import MenuEntry from "../../domain/model/MenuEntry";


const MenuPresenter = (() => {

    let _menusView;
    let _editMenuView;
    let _restaurantsDropDownView;

    const bindView = (view) => {
        _menusView = view;
    }

    const bindViewEditMenuView = (view) => {
        _editMenuView = view;
    }

    const presentMenu = (response = {menuEntries: [], cart: []}) => {
        const viewModel = [];
        response.menuEntries.forEach(
            (menuEntry) => {      
                viewModel.push({
                    foodId: menuEntry.foodId,
                    dishName: menuEntry.dishName,
                    price: menuEntry.price,
                    dishDescription: menuEntry.dishDescription,
                    imgUrl: menuEntry.imgUrl,
                    existsInCart: _existInCart(menuEntry.foodId, response.cart)
                })
            }
        )
        _menusView.displayMenu(viewModel, response.curCartRestaurantId);
    }

    const _existInCart = (foodId, cart = []) => {
        for(const cartEntry of cart) {
            if(foodId === cartEntry.foodId) return true;
        }
        return false;
    }

    const presentRestaurants = (response = {restaurants: [], restaurant: {}}) => {
        console.log(response)
        _restaurantsDropDownView.displayRestaurantsDropdown(response.restaurants, response.restaurant)
    }

    const hideEditMenuView = () => {
        _menusView.hideEditMenuView();
    }

    const presentEditMenu = (foodId, menuEntry) => {
        if (menuEntry) {
            //Uppdate view
            _editMenuView?.updateViewState(
                menuEntry.foodId,
                menuEntry.dishName,
                menuEntry.price,
                menuEntry.dishDescription,
                menuEntry.imgUrl
            )
        }
        else {
            //Open view
            _menusView?.displayEditMenuView(foodId);
        }
    }

    const bindRestaurantsDropDownView = (view) => {
        _restaurantsDropDownView = view;
    }

    const presentUpdateMenu = (id) => {
        hideEditMenuView();
        _menusView.rerender();
    }

    return {
        bindView,
        bindViewEditMenuView,
        presentMenu,
        hideEditMenuView,
        presentEditMenu,
        presentUpdateMenu,
        bindRestaurantsDropDownView,
        presentRestaurants,
    }

}) ();

export default MenuPresenter;