

const EmployeesPresenter = (() => {

    let _employeesView;
    let _editEmployeeView;

    const bindView = (view) => {
        _employeesView = view;
    }

    const bindViewEditEmployeeView = (view) => {
        _editEmployeeView = view;
    }

    const presentEmployees = (response = {employees: []}) => {
        const viewModel = [];
        response.employees.forEach(
            (employee) => {
                viewModel.push({
                    id: employee.employeeNumber,
                    name: employee.name,
                    age: employee.age,
                    email: employee.email,
                    tlfnr: employee.tlfnr,
                    imgUrl: employee.imgUrl,
                    salary: employee.salary,
                    title: employee.title
                })
            }
        )
        _employeesView.displayEmployees(viewModel);
    }

    const hideEditEmployeeView = () => {
        _employeesView.hideEditEmployeeView();
    }

    const presentEditEmployee = (id, employee) => {
        if (employee) {
            //Update view
            _editEmployeeView?.updateViewState(
                employee.id,
                employee.name,
                employee.age,
                employee.email,
                employee.tlfnr,
                employee.imgUrl,
                employee.salary,
                employee.title
            )
        }
        else {
            //Open view
            _employeesView?.displayEditEmployeeView(id);
        }
    }

    const presentUpdateEmployees = () => {
        hideEditEmployeeView();
        _employeesView?.rerender();
    }

    return {
        bindView,
        bindViewEditEmployeeView,
        presentEmployees,
        presentEditEmployee,
        hideEditEmployeeView,
        presentUpdateEmployees
    }
}) ();

export default EmployeesPresenter;