

const CheckoutPresenter = (() => {

    let _checkoutView;
    let _checkoutsCartViewWidget;
    let _checkoutsLogViewWidget;

    const bindView = (view) => {
        _checkoutView = view;
    }

    const bindCheckoutsLogViewWidget = (viewInstance) => {
        _checkoutsLogViewWidget = viewInstance;
    }

    const bindCheckoutsCartViewWidget = (viewInstance) => {
        _checkoutsCartViewWidget = viewInstance;
    }

    const presentCurrentCart = (response = {restaurant: {}, cart: []}) => {
        _checkoutsCartViewWidget.displayCart(response.restaurant, response.cart)
    }

    const presentEditCheckout = () => {
        _checkoutView.displayEditCheckout();
    }

    const presentExitEditCheckout = () => {
        presentEditCheckout();
    }

    const presentCheckoutLog = (checkoutLog = []) => {
        console.log(checkoutLog)
        _checkoutsLogViewWidget?.displayCheckoutLog(checkoutLog);
    }

    return {
        bindView,
        bindCheckoutsLogViewWidget,
        bindCheckoutsCartViewWidget,
        presentCurrentCart,
        presentEditCheckout,
        presentExitEditCheckout,
        presentCheckoutLog
    }
}) ();

export default CheckoutPresenter;