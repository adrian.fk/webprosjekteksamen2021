import StatisticsPresenter from "../presenters/StatisticsPresenter";
import RestaurantRepository from "../repositories/RestaurantRepository";
import StatisticsRepository from "../repositories/StatisticsRepository";
import MenuRepository from "../repositories/MenuRepository";


const StatisticsController = () => {

    const initStatistics = (view) => {
        StatisticsPresenter.bindStatisticsView(view);
    }

    const initRestaurants = (view) => {
        StatisticsPresenter.bindRestaurantsViewWidget(view);
        fetchRestaurantsInfo();
    }

    const initSales = (view) => {
        StatisticsPresenter.bindSalesViewWidget(view);
        fetchSalesInfo();
    }

    const initEconomics = (view) => {
        StatisticsPresenter.bindEconomicsViewWidget(view);
        fetchEconomicsStats();
    }

    const fetchEconomicsStats = () => {
        StatisticsRepository().getTotalRevenue()
            .then(totalRevenue => {
                StatisticsRepository().getTotalSalary()
                    .then(totalSalary => {
                        StatisticsPresenter.presentEconomicsStatistics({totalRevenue, totalSalary})
                    })
            })
    }

    const fetchSalesInfo = () => {
        StatisticsRepository().getMostSoldMenuEntry()
            .then(mostSold => {
                StatisticsRepository().getAvgPriceSoldItems()
                    .then(totalAvgSold => {
                        MenuRepository().getIdToMenuEntryMap()
                            .then(idToMenuEntryMap => {
                                const mostSoldEntry = {...mostSold, dishName: idToMenuEntryMap.get(mostSold.foodId).dishName}
                                RestaurantRepository().fetchRestaurants()
                                    .then(
                                        restaurants => {
                                            const resRestaurants = [];

                                            for (let i = 0; i < restaurants.length; i++) {
                                                const restaurant = restaurants[i];
                                                StatisticsRepository().getTotalRevenueByRestaurantId(restaurant.restaurantId)
                                                    .then(totalSold => {
                                                        resRestaurants.push({...restaurant, totalSold});
                                                        if (i === restaurants.length - 1) {
                                                            const response = {mostSoldEntry, totalAvgSold, resRestaurants}
                                                            StatisticsPresenter.presentSalesStats(response)
                                                        }
                                                    })
                                            }
                                        }
                                    )
                            })
                    })
            })
    }

    const fetchRestaurantsInfo = () => {
        RestaurantRepository().fetchRestaurants()
            .then((inputRestaurants) => {
                const restaurants = [];

                //Skrell av det som er empty sånn at følgende loop bare eksekverer for de restaurantene som skal vurderes
                //og mostSoldItemsAtRestaurant.length er lengden av de man skal vurdere.


                //Pealing off those restaurants that should not be considered, because they're empty.
                for (let restaurantsIterator = 0; restaurantsIterator < inputRestaurants.length; restaurantsIterator++) {
                    const restaurant = inputRestaurants[restaurantsIterator];
                    StatisticsRepository().getMostSoldItemsByRestaurantId(restaurant.restaurantId)
                        .then(mostSoldItemsAtRestaurant => {
                            if (mostSoldItemsAtRestaurant.length) restaurants.push(restaurant);
                        })
                        .finally(
                            () => {
                                if (restaurantsIterator === inputRestaurants.length - 1) {
                                    const response = {restaurants, mostSoldItemsPerRestaurant: [], avgPriceSoldByRestaurant: []};
                                    for (let i = 0; i < restaurants.length; i++) {
                                        const restaurant = restaurants[i];
                                        StatisticsRepository().getAvgPriceSoldByRestaurant(restaurant.restaurantId)
                                            .then(
                                                (avgPriceSoldRestaurant) => {
                                                    StatisticsRepository().getMostSoldItemsByRestaurantId(restaurant.restaurantId)
                                                        .then(
                                                            (mostSoldItemsAtRestaurant) => {
                                                                let mostSoldItems = [];
                                                                console.log(mostSoldItemsAtRestaurant)
                                                                if (mostSoldItemsAtRestaurant.length === 0) {
                                                                    if (i === restaurants.length - 1) {
                                                                        //Present
                                                                        StatisticsPresenter.presentRestaurantStatistics(response);
                                                                    }
                                                                }
                                                                else
                                                                    for (let e = 0; e < mostSoldItemsAtRestaurant.length; e++) {
                                                                        const mostSoldItem = mostSoldItemsAtRestaurant[e];
                                                                        MenuRepository().getIdToMenuEntryMap()
                                                                            .then(
                                                                                (map) => {
                                                                                    mostSoldItems.push({
                                                                                        foodId: mostSoldItem.foodId,
                                                                                        soldUnits: mostSoldItem.soldUnits,
                                                                                        name: map.get(mostSoldItem.foodId).dishName
                                                                                    })
                                                                                    if (e === mostSoldItemsAtRestaurant.length - 1) {
                                                                                        response.avgPriceSoldByRestaurant.push({restaurantId: restaurant.restaurantId, avgPriceSoldRestaurant});
                                                                                        response.mostSoldItemsPerRestaurant.push({restaurantId: restaurant.restaurantId, mostSoldItems})
                                                                                        if (i === restaurants.length - 1) {
                                                                                            //Present
                                                                                            StatisticsPresenter.presentRestaurantStatistics(response);
                                                                                        }
                                                                                    }
                                                                                }
                                                                            )
                                                                    }
                                                            }
                                                        )
                                                }
                                            )
                                    }
                                }
                            }
                        )
                }

            })
    }

    return {
        initStatistics,
        initRestaurants,
        initEconomics,
        initSales,
        fetchSalesInfo,
        fetchRestaurantsInfo
    }
}

export default StatisticsController;

