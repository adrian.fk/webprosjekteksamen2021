import RestaurantRepository from "../repositories/RestaurantRepository";
import EmployeesRepository from "../repositories/EmployeesRepository";
import RestaurantsPresenter from "../presenters/RestaurantsPresenter";


const RestaurantsController = () => {

    const init = (view) => {
        RestaurantsPresenter.bindView(view);
        fetchRestaurants();
    }

    const initEditRestaurant = (id, view) => {
        RestaurantsPresenter.bindViewEditRestaurantView(view);
        fetchRestaurant(id).then((restaurant) => {
            RestaurantsPresenter.presentEditRestaurant(restaurant)
        });
    }

    const fetchRestaurants = () => {
        RestaurantRepository().fetchRestaurants()
            .then((restaurants) => {
                EmployeesRepository().fetchEmployees()
                    .then(
                        (employees) => {
                            RestaurantsPresenter.presentRestaurants({restaurants, employees});
                        }
                    )
            });
    }

    const fetchRestaurant = (id) => new Promise(
        (resolve) => {
            RestaurantRepository().getRestaurant(id)
                .then(restaurant => {
                    RestaurantsPresenter.presentEditRestaurant(id, restaurant)
                    resolve(restaurant);
                })
        }
    )

    const onRestaurantClicked = (id) => {
        RestaurantsPresenter.presentEditRestaurant(id)
    }

    const onExitEditRestaurantView = () => {
        RestaurantsPresenter.presentUpdateRestaurants();
    }

    const onUpdateRestaurant = (restaurantId, name, imgUrl, tlfnr, address, managerEmployeeNum) => {
        RestaurantRepository()
            .updateRestaurant(restaurantId, name, imgUrl, tlfnr, address, managerEmployeeNum)
            .then(res => RestaurantsPresenter.presentUpdateRestaurants(restaurantId));

    }

    const onAddRestaurant = (name, imgUrl, tlfnr, address, managerEmployeeNum) => {
        RestaurantRepository().addNewRestaurant(name, imgUrl, tlfnr, address, managerEmployeeNum)
            .then((restaurant) => {RestaurantsPresenter.presentEditRestaurant(restaurant.restaurantId)})
    }

    const onRemoveRestaurant = (restaurantId) => {
        RestaurantRepository().removeRestaurant(restaurantId);
        RestaurantsPresenter.presentUpdateRestaurants(restaurantId);
    }


    return {
        init,
        initEditRestaurant,
        fetchRestaurants,
        fetchRestaurant,
        onRestaurantClicked,
        onExitEditRestaurantView,
        onUpdateRestaurant,
        onAddRestaurant,
        onRemoveRestaurant
    }
}

export default RestaurantsController;