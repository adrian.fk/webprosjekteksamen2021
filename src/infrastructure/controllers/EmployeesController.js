import EmployeesRepository from "../repositories/EmployeesRepository";
import EmployeesPresenter from "../presenters/EmployeesPresenter";


const EmployeesController = () => {

    const init = (view) => {
        EmployeesPresenter.bindView(view);
        fetchEmployees();
    }

    const initEditEmployee = (id, view) => {
        EmployeesPresenter.bindViewEditEmployeeView(view);
        fetchEmployee(id).then((employee) => {
            EmployeesPresenter.presentEditEmployee(employee)
        });
    }

    const fetchEmployees = () => {
        EmployeesRepository().fetchEmployees()
            .then((employees) => {
                EmployeesPresenter.presentEmployees({employees});
            });
    }

    const fetchEmployee = (id) => new Promise(
        (resolve) => {
            EmployeesRepository().getEmployee(id)
                .then(employee => {
                    EmployeesPresenter.presentEditEmployee(id, employee)
                    resolve(employee);
                })
        }
    )

    const onEmployeeClicked = (id) => {
        EmployeesPresenter.presentEditEmployee(id);
    }

    const onExitEditEmployeeView = () => {
        EmployeesPresenter.presentUpdateEmployees();
    }

    const onUpdateEmployee = (employeeNumber, name, age, email, tlfnr, imgUrl, salary, title) => {
        EmployeesRepository()
            .updateEmployee(employeeNumber, name, age, email, tlfnr, imgUrl, salary, title)
            .then(emp => EmployeesPresenter.presentUpdateEmployees(employeeNumber));
    }

    const onAddEmployee = (employeeNumber, name, age, email, tlfnr, imgUrl, salary, title) => {
        EmployeesRepository().addNewEmployee(employeeNumber, name, age, email, tlfnr, imgUrl, salary, title)
            .then((employee) => {EmployeesPresenter.presentEditEmployee(employee.employeeNumber)})
    }

    const onRemoveEmployee = (employeeNumber) => {
        EmployeesRepository().removeEmployee(employeeNumber);
        EmployeesPresenter.presentUpdateEmployees(employeeNumber);
    }


    return {
        init,
        initEditEmployee,
        fetchEmployees,
        fetchEmployee,
        onEmployeeClicked,
        onExitEditEmployeeView,
        onUpdateEmployee,
        onAddEmployee,
        onRemoveEmployee
    }
}

export default EmployeesController