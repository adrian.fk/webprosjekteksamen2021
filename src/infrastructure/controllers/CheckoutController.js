import CheckoutPresenter from "../presenters/CheckoutPresenter"
import CheckoutRepository from "../repositories/CheckoutRepository";
import ShoppingCartRepository from "../repositories/ShoppingCartRepository";
import RestaurantRepository from "../repositories/RestaurantRepository";


const CheckoutController = () => {
    
    const init = (view) => {
        CheckoutPresenter.bindView(view);
    }

    const initCheckoutsCartViewWidget = (viewInstance) => {
        CheckoutPresenter.bindCheckoutsCartViewWidget(viewInstance);
        fetchCurrentCart();
    }

    const initCheckoutsLogViewWidget = (viewInstance) => {
        CheckoutPresenter.bindCheckoutsLogViewWidget(viewInstance);
        fetchCheckoutLog()
    }

    const fetchCurrentCart = () =>  {
        ShoppingCartRepository().getShoppingCart()
            .then(cart => {
                RestaurantRepository().getRestaurant(cart.restaurantID)
                    .then(restaurant => {
                        console.log(cart)
                        CheckoutPresenter.presentCurrentCart({restaurant, cart: cart.menuEntries});
                    })
            })
    }

    const fetchCheckoutLog = () => {
        CheckoutRepository().getCheckouts()
            .then(
                checkouts => {
                    console.log(checkouts)
                    const checkoutsArr = [];
                    for (let i = 0; i < checkouts.length; i++) {
                        const checkout = checkouts[i];
                        console.log(checkout)
                        RestaurantRepository().getRestaurant(checkout.restaurantId)
                            .then(restaurant => {
                                checkoutsArr.push({...checkout, restaurant});
                                console.log(restaurant)
                                if (checkoutsArr.length === checkouts.length) {
                                    CheckoutPresenter.presentCheckoutLog(checkoutsArr)
                                }
                            })
                    }
                }
            )
    }

    const onSubmitCart = () => {
        ShoppingCartRepository().submitShoppingCart(
            () => {
                fetchCurrentCart();
                fetchCheckoutLog();
            }
        )
    }

    const onEditInit = () => {
        CheckoutPresenter.presentEditCheckout();
    }

    const onSubmitRestaurantId = (restaurantId) => {
        ShoppingCartRepository().updateRestaurantID(restaurantId)
            .then(
                () => {
                    //Present close + rerender
                    CheckoutPresenter.presentExitEditCheckout();
                    fetchCurrentCart();
                }
            )
    }

    const onRemoveShoppingCartItem = (id) => {
        ShoppingCartRepository().removeMenuEntry(id);
        fetchCurrentCart();
    }

    const onExitEditCheckout = () => {
        CheckoutPresenter.presentExitEditCheckout();
    }


    return {
        init,
        onEditInit,
        initCheckoutsCartViewWidget,
        initCheckoutsLogViewWidget,
        fetchCurrentCart,
        fetchCheckoutLog,
        onSubmitCart,
        onRemoveShoppingCartItem,
        onSubmitRestaurantId,
        onExitEditCheckout
    }
}

export default CheckoutController;

