import MenuPresenter from "../presenters/MenuPresenter";
import MenuRepository from "../repositories/MenuRepository";
import RestaurantRepository from "../repositories/RestaurantRepository";
import ShoppingCartRepository from "../repositories/ShoppingCartRepository";


const MenuController = () => {

    const init = (view) => {
        MenuPresenter.bindView(view);
        fetchMenu();
    }

    const initRestaurantsDropDown = (view) => {
        MenuPresenter.bindRestaurantsDropDownView(view);
        fetchRestaurants();
    }

    const initEditMenu = (id, view) => {
        MenuPresenter.bindViewEditMenuView(view);
        fetchDishForEditSection(id).then((menu) => {
            MenuPresenter.presentEditMenu(id, menu)
        });
    }

    const fetchRestaurants = () => {
        RestaurantRepository().fetchRestaurants()
            .then(restaurants => {
                ShoppingCartRepository().getRestaurantId()
                    .then(restaurantId => {
                        RestaurantRepository().getRestaurant(restaurantId)
                            .then(restaurant => {
                                MenuPresenter.presentRestaurants({restaurants, restaurant});
                            })
                    })
            })
    }

   const fetchMenu = () => {
        MenuRepository().getMenuEntries()
            .then((menu) => {
                ShoppingCartRepository().getMenuEntries()
                    .then(
                        (menuEntries) => {
                            ShoppingCartRepository().getRestaurantId()
                                .then(curCartRestaurantId => {
                                    console.log({menuEntries: menu, cart: menuEntries, curCartRestaurantId});
                                    MenuPresenter.presentMenu({menuEntries: menu, cart: menuEntries, curCartRestaurantId})
                                })
                        }
                    ) 
            })
    }

    const fetchDishForEditSection = (foodId) => new Promise(
        (resolve) => {
            MenuRepository().getMenuEntry(foodId)
                .then(menu => {
                    resolve(menu);
                })
        }
    )

    const onMenuClicked = (foodId) => {
        fetchDishForEditSection(foodId).then(menuEntry => MenuPresenter.presentEditMenu(foodId));
    }

    const onExitEditMenuView = () => {
        MenuPresenter.hideEditMenuView();
    }

    const onUpdateMenuEntry = (foodId, dishName, price, dishDescription, imgUrl) => {
        MenuRepository()
            .updateMenuEntry(foodId, dishName, price, dishDescription, imgUrl)
            .then(menuEntry => MenuPresenter.presentUpdateMenu(menuEntry.foodId));
    }

    const onAddMenuEntry = () => {
        MenuRepository().addNewMenuEntry("", "", "", "")
            .then((menu) => {
                MenuPresenter.presentEditMenu(menu.foodId)
            })
    }

    const setRestaurantId = (restaurantId) => {
        ShoppingCartRepository().updateRestaurantID(restaurantId)
            .then(() => fetchMenu())
    }

    const onRemoveMenuEntry = (foodId) => {
        MenuRepository().removeMenuEntry(foodId);
        MenuPresenter.presentUpdateMenu(foodId);
    }

    const onAddShoppingCart = (foodId, dishName, price, dishDescription, imgUrl, restaurantId) => {
        ShoppingCartRepository().addMenuEntry(restaurantId, {foodId, dishName, price, dishDescription, imgUrl})
        .then(
            () => {
                fetchMenu()
            }
        );
    }

    
    const onRemoveShoppingCart = (foodId) => {
        ShoppingCartRepository().removeMenuEntry(foodId);
        fetchMenu();
    }
      
    return {
        init,
        initEditMenu,
        fetchMenu,
        onAddMenuEntry,
        onRemoveMenuEntry,
        fetchDishForEditSection,
        onMenuClicked,
        onExitEditMenuView,
        onUpdateMenuEntry,
        onAddShoppingCart,
        onRemoveShoppingCart,
        initRestaurantsDropDown,
        setRestaurantId
    }
}

export default MenuController;