import DataEmployees from "../../data/DataEmployees";


const EmployeesRepository = () => {

    const init = () => DataEmployees.init();

    /*
    const fetchEmployees = () => {
        return new Promise(
            resolve => {
                DataEmployees.getEmployees()
                    .then((employees) => resolve(employees));
            }
        );
    }
    */

    const fetchEmployees = () => new Promise(
        resolve => {
            DataEmployees.initIfNecessary()
                .then(() => resolve(DataEmployees.getEmployees()))
        }
    )

    const addEmployee = (employeeNumber, name, age, email, tlfnr, imgUrl, salary, title) => {
        DataEmployees.addEmployee(employeeNumber, name, age, email, tlfnr, imgUrl, salary, title)
    }

    const updateEmployee = (employeeNumber, name, age, email, tlfnr, imgurl, salary, title) => new Promise(
        resolve => {
            const out = DataEmployees.updateEmployee(employeeNumber, name, age, email, tlfnr, imgurl, salary, title);
            resolve(out);
        }
    )

    const removeEmployee = (employeeNumber) => {
        DataEmployees.removeEmployee(employeeNumber);
    }

    const getEmployee = (employeeNumber) => new Promise(
        (resolve) => {
            resolve(DataEmployees.getEmployee(employeeNumber))
        }
    )

    const addNewEmployee = (employeeNumber, name, age, email, tlfnr, imgurl, salary, title) => new Promise(
        resolve => {
            resolve(DataEmployees.addNewEmployee(employeeNumber, name, age, email, tlfnr, imgurl, salary, title))
        }
    )

    return {
        init,
        fetchEmployees,
        addEmployee,
        updateEmployee,
        removeEmployee,
        getEmployee,
        addNewEmployee
    }
}

export default EmployeesRepository;