import DataMenuEntries from "../../data/DataMenuEntries";


const MenuRepository = () => {

    const init = () => DataMenuEntries.init();

    const getMenuEntries = () => {
        return new Promise(
            (resolve) => {
                DataMenuEntries.getMenuEntriesArr()
                    .then((arr) => resolve(arr));
            }
        )
    }

    const getIdToMenuEntryMap = () => {
        return new Promise(
            (resolve) => {
                DataMenuEntries.getMenuEntriesMap()
                    .then((map) => resolve(map));
            }
        )
    }

    const getMenuEntry = (foodId) => new Promise (
        (resolve, reject) => {
            getIdToMenuEntryMap()
                .then(map =>{ 
                    resolve(map.get(foodId));
                })
                .catch(error => reject(error));
        }
    )
    
    const addNewMenuEntry = (dishName, price, dishDescription, imgUrl) => new Promise (
        (resolve) => {
            DataMenuEntries.addNewMenuEntry(dishName, price, dishDescription, imgUrl)
                .then(newEntry => {
                    resolve(newEntry)
                })
        })

    const removeMenuEntry = (foodId) => {
        DataMenuEntries.removeMenuEntry(foodId);
    }

    const updateMenuEntry = (foodId, dishName, price, dishDescription, imgUrl) => new Promise (
        (resolve) => {
            resolve(
                DataMenuEntries.updateMenuEntry(foodId, dishName, price, dishDescription, imgUrl)
            )
        }
    )

    const addMenuEntry = (foodId, dishName, price, dishDescription, imgUrl) => new Promise (
        (resolve) => {
            DataMenuEntries.addMenuEntry(foodId, dishName, price, dishDescription, imgUrl)
            resolve()
        }
    )

    return {
        init,
        addNewMenuEntry,
        removeMenuEntry,
        getMenuEntries,
        updateMenuEntry,
        getIdToMenuEntryMap,
        getMenuEntry,
        addMenuEntry
    }
}

export default MenuRepository;