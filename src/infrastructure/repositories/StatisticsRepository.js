import DataStatistics from "../../data/DataStatistics";
import DataShoppingCart from "../../data/DataShoppingCart";


const StatisticsRepository = () => {

    const commitShoppingCart = () => {
        const currentCart = {cartRestaurantId: DataShoppingCart.getRestaurantId, cartEntries: DataShoppingCart.getMenuEntries()}
        DataStatistics.commitShoppingCart();
        return currentCart;
    }

    const getAllOrdersAtOneRestaurant = (restaurantId) => {
        return new Promise(
            (resolve) => {
                resolve(DataStatistics.getAllOrdersArr(restaurantId))
            }
        )
    }

    const getTotalRevenueByRestaurantId = (restaurantId) => {
        return new Promise(
            resolve => {
                DataStatistics.getTotalRevenueByRestaurant(restaurantId)
                    .then(totalRevenueRestaurant => resolve(totalRevenueRestaurant))
            }
        )
    }

    const getTotalRevenue = () => new Promise(
        resolve => {
            DataStatistics.getTotalRevenue()
                .then(totalRevenue => {
                    resolve(totalRevenue);
                })
        }
    )

    const getAvgPriceSoldByRestaurant = (restaurantId) => new Promise(
        resolve => {
            DataStatistics.getAvgPriceByRestaurant(restaurantId)
                .then(avgPrice => {
                    resolve(avgPrice)
                });
        }
    );

    const getAvgPriceSoldItems = () => new Promise(
        resolve => resolve(DataStatistics.getAvgPrice())
    )

    const getNumberOfRestaurants = () => DataStatistics.getNumberOfRestaurants();

    const getNumMenuEntries = () => new Promise(
        resolve => resolve(DataStatistics.getNumberOfMenuEntries())
    )

    const getMostSoldArr = () => new Promise(
        resolve => {
            DataStatistics.getMostSoldArr()
                .then(mostSoldArr => resolve(mostSoldArr))
        }
    )

    const getMostSoldItemsByRestaurantId = (restaurantId) => new Promise(
        resolve => {
            DataStatistics.getMostSoldArrByRestaurant(restaurantId)
                .then(mostSoldArr => {
                    resolve(mostSoldArr)
                })
        }
    )

    const getMostSoldMenuEntry = () => new Promise(
        resolve => resolve(DataStatistics.getMostSoldMenuEntry())
    )

    const getTotalSalary = () => new Promise(
        resolve => {
            DataStatistics.getTotalSalary()
                .then(totalSalary => resolve(totalSalary))
        }
    )

    return {
        commitShoppingCart,
        getAllOrdersAtOneRestaurant,
        getTotalRevenueByRestaurantId,
        getTotalRevenue,
        getAvgPriceSoldByRestaurant,
        getAvgPriceSoldItems,
        getNumberOfRestaurants,
        getNumMenuEntries,
        getMostSoldArr,
        getMostSoldItemsByRestaurantId,
        getMostSoldMenuEntry,
        getTotalSalary
    }
}

export default StatisticsRepository;