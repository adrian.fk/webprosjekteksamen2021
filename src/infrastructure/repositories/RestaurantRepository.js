import DataRestaurants from "../../data/DataRestaurants";


const RestaurantRepository = () => {

    const init = () => DataRestaurants.init();

    const fetchRestaurants = () => new Promise(
        resolve => {
            DataRestaurants.getRestaurants()
                .then((restaurants) => resolve(restaurants))
        }
    )

    const addRestaurant = (restaurantId, name, imgUrl, tlfnr, address, manager) => {
        DataRestaurants.addRestaurant(restaurantId, name, imgUrl, tlfnr, address, manager);
    }

    const updateRestaurant = (restaurantId, name, imgUrl, tlfnr, address, managerEmployeeNum) => new Promise(
        resolve => {
            const out = DataRestaurants.updateRestaurant(restaurantId, name, imgUrl, tlfnr, address, managerEmployeeNum);
            resolve(out);
        }
    )

    const removeRestaurant = (restaurantId) => {
        DataRestaurants.removeRestaurant(restaurantId);
    }

    const getRestaurant = (id) => new Promise(
        (resolve) => {
            DataRestaurants.getRestaurant(id)
                .then((restaurant) => resolve(restaurant))
        }
    )

    const addNewRestaurant = (name, imgUrl, tlfnr, address, managerEmployeeNum) => new Promise(
        resolve => {
            resolve(DataRestaurants.addNewRestaurant(name, imgUrl, tlfnr, address, managerEmployeeNum));
        }
    )

    return {
        init,
        fetchRestaurants,
        addRestaurant,
        updateRestaurant,
        removeRestaurant,
        getRestaurant,
        addNewRestaurant
    }
}

export default RestaurantRepository;