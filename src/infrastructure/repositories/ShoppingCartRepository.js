import DataShoppingCart from "../../data/DataShoppingCart";
import DataStatistics from "../../data/DataStatistics";


const ShoppingCartRepository = () => {

    const addMenuEntry = (restaurantId, menuEntry) => {
        return new Promise(
            resolve => {
                    DataShoppingCart.addMenuEntry(restaurantId, menuEntry)
                resolve({restaurantId, menuEntry});
            }
        )

    }

    const clearCart = () => {
        DataShoppingCart.clearCart();
    }

    const removeMenuEntry = (menuEntryId) => {
        DataShoppingCart.removeMenuEntry(menuEntryId);
    }

    const getShoppingCart = () => new Promise(
        resolve => {
            DataShoppingCart.getShoppingCart()
                .then((cart) => {
                    resolve(cart);
                });
        }
    )


    const getRestaurantId = () => new Promise(
        resolve => resolve(DataShoppingCart.getRestaurantId())
    )

    const getMenuEntries = () => {
        return new Promise(
            resolve => {
                resolve(DataShoppingCart.getMenuEntries());
            }
        )
    }

    const submitShoppingCart = (callback) => {
        DataStatistics.commitShoppingCart()
            .then(() => {
                if (callback) callback()
            })
    }

    const updateRestaurantID = (restaurantId) => new Promise(
        resolve => {
            DataShoppingCart.setRestaurantId(restaurantId)
            resolve();
        }
    )


    return {
        addMenuEntry,
        clearShoppingCart: clearCart,
        removeMenuEntry,
        getShoppingCart,
        getRestaurantId,
        getMenuEntries,
        submitShoppingCart,
        updateRestaurantID
    }
}

export default ShoppingCartRepository;