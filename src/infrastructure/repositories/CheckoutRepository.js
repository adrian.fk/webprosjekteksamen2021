import DataCheckouts from "../../data/DataCheckouts";


const CheckoutRepository = () => {

    const getCheckouts = () => {
        return new Promise(
            resolve => {
                DataCheckouts.getCheckoutsArr()
                    .then((arr) => {
                        resolve(arr)
                    });
            }
        )
    }

    const getRestaurantIdToCheckoutsMap = () => {
        return new Promise(
            resolve => {
                DataCheckouts.getCheckoutsMap()
                    .then((restaurantIdToCheckoutMap) => {
                        console.log(restaurantIdToCheckoutMap)
                        resolve(restaurantIdToCheckoutMap)
                    });
            }
        )
    }

    const getCheckoutIdToCheckoutsMap = () => {
        return new Promise(
            resolve => {
                DataCheckouts.getCheckoutIdToCheckoutMap()
                    .then((checkoutIdToCheckoutMap) => resolve(checkoutIdToCheckoutMap))
            }
        )
    }

    const addCheckout = (restaurantId, menuEntries) => {
        DataCheckouts.addCheckout(restaurantId, menuEntries);
    }



    return {
        getCheckouts,
        getRestaurantIdToCheckoutsMap,
        addCheckout,
        getCheckoutIdToCheckoutsMap
    }
}
export default CheckoutRepository;